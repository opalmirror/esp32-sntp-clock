#!/usr/bin/python3
# Ubuntu package prerequisites: python3-pyqt5 python3-pyqtgraph python3-paho-mqtt

import numpy as np
import json
from pathlib import Path
from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import QApplication, QMainWindow, QComboBox, QToolBar
from pyqtgraph import AxisItem, PlotWidget, mkPen, plot
from sys import argv, exit # We need sys so that we can pass argv to QApplication
from paho.mqtt.client import Client as MQTTClient
from time import time as gettime
from time import mktime
from datetime import datetime, timedelta
from argparse import ArgumentParser

topic_default = '/hopewood/office/#'
host_default = 'basalt'
cur_name_default = 'temp'
log_default = False

debug = False
save_data_log = log_default
load_fname = 'all_topics.log'
save_fname = 'all_topics.log'

# stores named Topic class instances
topics = dict()

# useful constants
s_per = {
    'second': 1,
    '2seconds': 2,
    '10seconds': 10,
    '20seconds': 20,
    'minute': 60,
    '2minutes': 2 * 60,
    '10minutes': 10 * 60,
    '20minutes': 20 * 60,
    'hour': 60 * 60,
    '2hours': 2 * 60 * 60,
    '8hours': 8 * 60 * 60,
    'day': 24 * 60 * 60,
    '2days': 2 * 24 * 60 * 60,
    'week': 7 * 24 * 60 * 60,
    '2weeks': 14 * 24 * 60 * 60,
    'month': 31 * 24 * 60 * 60,
    '2months': (30 + 31) * 24 * 60 * 60,
    'quarter': (30 + 31 + 31) * 24 * 60 * 60,
    'year': 366 * 24 * 60 * 60,
    '2years': (365 + 366) * 24 * 60 * 60
}

# display periods
periods = ('minute', '10minutes', 'hour', '8hours', 'day', '2days', 'week', '2weeks', 'month', 'quarter', 'year')

# default display period is the 'hour' period entry above
period_default = 'hour'

# new_data accumulates seconds, period, description, and data value
new_data = dict()

# q stores new_data entries for the MainWindow periodic update
q = list()

# Topic stores information about a statistic topic
# namely, name, description, period and data entries (time, value)

class Topic():

    # create with a name,
    def __init__(self, name):
        self.name = name
        self.description = name

        self.new = False

        # data[] contains (time, value) pairs
        self.data = dict()
        self.new_data = dict()

    def get_name(self):
        return self.name

    # indicate whether this is restored data (new=False) or novel (new=True)
    def set_description(self, desc):
        if self.description != desc:
            self.new = True

        self.description = desc

    def is_new(self):
        return self.new

    def set_new(self, new):
        self.new = new

    def get_description(self):
        return self.description

    def add_old_data(self, time, value):
        self.data[int(time)] = float(value)

    def add_new_data(self, time, value):
        new_time = int(time) not in self.data
        self.data[int(time)] = float(value)
        if new_time:
            self.new_data[int(time)] = float(value)

    def get_all_data(self):
        return dict(self.data)

    def get_new_data(self):
        d = dict(self.new_data)
        self.new_data = dict()
        return d

    def get_dict(self):
        d = dict()

        d['name'] = self.name
        d['description'] = self.description
        d['data'] = self.data

        return d

    def set_dict(self, d):
        self.name = d['name']
        self.description = d['description']
        self.data = d['data']

    def get_times_after(self, time):
        x = list()

        for t, v in self.data.items():
            if t >= time:
                x.append(t)

        return x

    def get_values_after(self, time):
        y = list()

        for t, v in self.data.items():
            if t >= time:
                y.append(v)

        return y

# sort history keys (times) and dump data values to file f as data lines
# returning count n

def save_data_lines(f, history):
    n = 0
    keys = list(history.keys())

    for k, v in sorted(history.items()):
        n = n + len(v.keys())

        f.write('data ' + str(k) + ' ')
        json.dump(v, f, sort_keys=True)
        f.write('\n')

    return n

# store data into history
# history is a dictionary keyed by time (seconds since epoch)
# containing values that are themselves a dictionary, keyed
# by topic name, with values containing the corresponding measurement
def add_data_to_history(history, tname, data):
    for k, v in data.items():
        if k not in history:
            history[k] = {tname: v}
        else:
            history[k].update({tname: v})

    return history

# save all measurements in topics[] to file
def save_all_topics(fname):
    n = 0

    if not save_data_log:
        return n

    with open(fname, 'w', encoding='utf8') as f:

        for tname in topics:
            if tname == 'microseconds' or tname == 'seconds':
                continue

            f.write('namedesc ' + tname + ' ' + topics[tname].get_description() + '\n')
            topics[tname].set_new(False)

        history = dict()

        for tname in topics:
            if tname == 'microseconds' or tname == 'seconds':
                continue

            data = topics[tname].get_all_data()
            history = add_data_to_history(history, tname, data)

        n = save_data_lines(f, history)

    return n

# save all new measurements in topics[] to end of file
def save_all_topics_new_data(fname):
    n = 0

    if not save_data_log:
        return n

    # if file does not exist, create it with a full save of all topics
    p = Path(fname)
    if not p.is_file():
        print(fname + ': creating')
        return save_all_topics(fname)

    with open(fname, 'a', encoding='utf8') as f:

        history = dict()

        for tname in topics:
            if tname == 'microseconds' or tname == 'seconds':
                continue

            # save any updates to names or description
            if topics[tname].is_new():
                f.write('namedesc ' + tname + ' ' + topics[tname].get_description() + '\n')
                topics[tname].set_new(False)

            data = topics[tname].get_new_data()
            history = add_data_to_history(history, tname, data)

        n = save_data_lines(f, history)

    return n

# restore all measurements from file into topics[]
def load_all_topics(fname):
    n = 0
    d = dict()

    try:
        with open(fname, 'r', encoding='utf8') as f:

            while True:
                line = f.readline()

                if not line:
                    break

                l = line.split(' ', 2)

                if l[0] == 'namedesc':
                    tname = l[1]
                    tdesc = l[2][:-1]

                    if tname not in topics:
                        topics[tname] = Topic(tname)

                    topics[tname].set_description(tdesc)
                    topics[tname].set_new(False)

                elif l[0] == 'data':
                    second = l[1]
                    data = l[2]

                    d = json.loads(data)

                    for tname, v in d.items():
                        if tname not in topics:
                            print(tname + ': bad topic name, cannot load')
                            exit(1)

                        topics[tname].add_old_data(second, v)
                        n = n + 1

                else:
                    print(fname + ': ' + line + ': syntax error, cannot load')
                    exit(1)

    except FileNotFoundError:
        print(fname + ': file not found, not loading')
        return -1

    return n

# From https://gist.github.com/cpascual/cdcead6c166e63de2981bc23f5840a98
# License: GPLv3
class DateAxisItem(AxisItem):
    _pxLabelWidth = 80

    def __init__(self, *args, **kwargs):
        AxisItem.__init__(self, *args, **kwargs)
        self._oldAxis = None

    def tickValues(self, minVal, maxVal, size):
        maxMajSteps = int(size/self._pxLabelWidth)

        dt1 = datetime.fromtimestamp(minVal)
        dt2 = datetime.fromtimestamp(maxVal)

        dx = maxVal - minVal
        majticks = []

        if dx > 2 * s_per['2years']:
            d = timedelta(days=366)
            for y in range(dt1.year + 1, dt2.year):
                dt = datetime(year=y, month=1, day=1)
                majticks.append(mktime(dt.timetuple()))

        elif dx > s_per['2months']:
            d = timedelta(days=31)
            dt = dt1.replace(day=1, hour=0, minute=0,
                             second=0, microsecond=0) + d
            while dt < dt2:
                # make sure that we are on day 1 (even if always sum 31 days)
                dt = dt.replace(day=1)
                majticks.append(mktime(dt.timetuple()))
                dt += d

        elif dx > s_per['2days']:
            d = timedelta(days=1)
            dt = dt1.replace(hour=0, minute=0, second=0, microsecond=0) + d
            while dt < dt2:
                majticks.append(mktime(dt.timetuple()))
                dt += d

        elif dx > s_per['2hours']:
            d = timedelta(hours=1)
            dt = dt1.replace(minute=0, second=0, microsecond=0) + d
            while dt < dt2:
                majticks.append(mktime(dt.timetuple()))
                dt += d

        elif dx > s_per['20minutes']:
            d = timedelta(minutes=10)
            dt = dt1.replace(minute=(dt1.minute // 10) * 10,
                             second=0, microsecond=0) + d
            while dt < dt2:
                majticks.append(mktime(dt.timetuple()))
                dt += d

        elif dx > s_per['2minutes']:
            d = timedelta(minutes=1)
            dt = dt1.replace(second=0, microsecond=0) + d
            while dt < dt2:
                majticks.append(mktime(dt.timetuple()))
                dt += d

        elif dx > s_per['20seconds']:
            d = timedelta(seconds=10)
            dt = dt1.replace(second=(dt1.second // 10) * 10, microsecond=0) + d
            while dt < dt2:
                majticks.append(mktime(dt.timetuple()))
                dt += d

        elif dx > s_per['2seconds']:
            d = timedelta(seconds=1)
            majticks = range(int(minVal), int(maxVal))

        else:  # <2s , use standard implementation from parent
            return AxisItem.tickValues(self, minVal, maxVal, size)

        L = len(majticks)
        if L > maxMajSteps:
            majticks = majticks[::int(np.ceil(float(L) / maxMajSteps))]

        return [(d.total_seconds(), majticks)]

    def tickStrings(self, values, scale, spacing):
        ret = []
        if not values:
            return []

        if spacing >= s_per['year']:
            fmt = "%Y"

        elif spacing >= s_per['month']:
            fmt = "%Y %b"

        elif spacing >= s_per['day']:
            fmt = "%b/%d"

        elif spacing >= s_per['hour']:
            fmt = "%b/%d-%Hh"

        elif spacing >= s_per['minute']:
            fmt = "%H:%M"

        elif spacing >= s_per['second']:
            fmt = "%H:%M:%S"

        else:
            # less than 2s (show microseconds)
            # fmt = '%S.%f"'
            fmt = '[+%fms]'  # explicitly relative to last second

        for x in values:
            try:
                t = datetime.fromtimestamp(x)
                ret.append(t.strftime(fmt))
            except ValueError:  # Windows can't handle dates before 1970
                ret.append('')

        return ret

    def attachToPlotItem(self, plotItem):
        """Add this axis to the given PlotItem
        :param plotItem: (PlotItem)
        """
        self.setParentItem(plotItem)
        viewBox = plotItem.getViewBox()
        self.linkToView(viewBox)
        self._oldAxis = plotItem.axes[self.orientation]['item']
        self._oldAxis.hide()
        plotItem.axes[self.orientation]['item'] = self
        pos = plotItem.axes[self.orientation]['pos']
        plotItem.layout.addItem(self, *pos)
        self.setZValue(-1000)

    def detachFromPlotItem(self):
        """Remove this axis from its attached PlotItem
        (not yet implemented)
        """
        raise NotImplementedError()  # TODO

# MainWindow implements the PyQt5 application's graphical window,
# graphic widgets and their stored data
class MainWindow(QMainWindow):

    def __init__(self, title, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

        # Restore topics[] data from file
        n = load_all_topics(load_fname)
        print(load_fname + ': loaded', n, 'measurements')

        ### Initialize all widgets

        # Top toolbar contains Topic and Period combo boxes
        self.cBTopic = QComboBox()
        self.cBTopic.setMinimumSize(150,10)
        self.cBTopic.addItem(cur_name_default)

        # add any missing topic names
        for t in topics:
            found = False

            for i in range(self.cBTopic.count()):
                if self.cBTopic.itemText(i) == t:
                    found = True

            if not found:
                self.cBTopic.addItem(t)

        self.cBPeriod = QComboBox()
        for desc in periods:
            self.cBPeriod.addItem(desc)
        self.cBPeriod.setCurrentIndex(periods.index(period_default))

        symbols = ('x', ' ')
        self.cBSymbol = QComboBox()
        for symbol in symbols:
            self.cBSymbol.addItem(symbol)
        self.cBSymbol.setCurrentIndex(0)

        pens = ('line', 'none')
        self.cBPen = QComboBox()
        for pen in pens:
            self.cBPen.addItem(pen)
        self.cBPen.setCurrentIndex(0)

        self.toolBarWidget = QToolBar()
        self.toolBarWidget.addWidget(self.cBTopic)
        self.toolBarWidget.addWidget(self.cBPeriod)
        self.toolBarWidget.addWidget(self.cBSymbol)
        self.toolBarWidget.addWidget(self.cBPen)

        self.addToolBar(self.toolBarWidget)

        # Graph plot widget is in center area
        self.styles = {"color": "#f00", "font-size": "20px"}

        self.graphWidget = PlotWidget()
        self.dateAxis = DateAxisItem(orientation = 'bottom')
        self.dateAxis.attachToPlotItem(self.graphWidget.getPlotItem())

        self.setCentralWidget(self.graphWidget)
        self.graphWidget.setBackground('w')
        self.graphWidget.setTitle(title, color='b', size='30pt')
        self.graphWidget.setLabel('left', self.cBTopic.currentText(), **self.styles)

        # set up bottom Label and X Range for graph
        cur_period = self.cBPeriod.currentIndex()
        period_name = periods[cur_period]
        period = s_per[period_name]

        now = gettime()
        self.graphWidget.setXRange(now - period, now, padding=0)

        self.graphWidget.addLegend()
        self.graphWidget.showGrid(x=True, y=True)
        #self.graphWidget.setYRange(20, 55, padding=0)

        # TODO use synchronous handler methods to handle combo box changes
        self.prev_name = self.cBTopic.currentText()
        self.prev_period = self.cBPeriod.currentIndex()

        ### Initialize the periodic update timer
        self.num_updates = 0
        self.timer = QTimer()
        self.timer.setInterval(250)
        self.timer.timeout.connect(self.update_plot_data)
        self.timer.start()

        self.last_data_time = 0
        self.data_to_save = False

    # period update callback
    def update_plot_data(self):
        now = gettime()

        must_save = False

        # add any data from mqtt callback q to the appropriate topic
        while len(q) > 0:
            data = q.pop(0)

            if debug:
                print('update:', data)

            topics[data['name']].add_new_data(data['seconds'], data['value'])

            self.last_data_time = now
            self.data_to_save = True

            found = False

            for i in range(self.cBTopic.count()):
                if self.cBTopic.itemText(i) == data['name']:
                    found = True

            # novel data topics get added to the topic name combo box
            if not found:
                self.cBTopic.addItem(data['name'])

        # save topics to file if new data has arrived
        if save_data_log and self.data_to_save and now > self.last_data_time + 1:
            self.data_to_save = False
            n = save_all_topics_new_data(save_fname)

            if debug:
                print(save_fname + ': saved', n, 'measurements')

        ### Update the display for current topic, period, and data

        cur_name = self.cBTopic.currentText()
        cur_period = self.cBPeriod.currentIndex()

        period_name = periods[cur_period]
        period = s_per[period_name]

        # set latest elements
        self.x = topics[cur_name].get_times_after(now - period)
        self.y = topics[cur_name].get_values_after(now - period)

        if self.num_updates == 0 or cur_name != self.prev_name or cur_period != self.prev_period:
            # First time, set the data

            # Change to topic name (description to left of graph)
            # Also, on initial update, set the description; it is available only
            # an MQTT description topic entry has been set.
            if self.num_updates == 0 or cur_name != self.prev_name:
                self.graphWidget.setLabel('left', topics[cur_name].get_description(), **self.styles)

            # Reflect change to data period.
            if cur_period != self.prev_period:
                self.graphWidget.setXRange(now - period, now, padding=0)

            # Plot the data.
            self.graphWidget.clear()

            if self.cBPen.currentIndex() == 0:
                pen = mkPen(color=(255, 0, 0))
            else:
                pen = None

            if self.cBSymbol.currentIndex() == 0:
                symbol = self.cBSymbol.currentText()
            else:
                symbol = None

            symbolPen=mkPen(color=(0,0,0))

            self.data_line = self.graphWidget.plot(self.x, self.y, pen=pen, symbol=symbol, symbolPen=symbolPen)
            self.data_line.setSymbolSize(1)
        else:
            # Merely update the data. Axes are all as before.
            self.data_line.setData(self.x, self.y)

        # finalize this update
        self.num_updates = self.num_updates + 1
        self.prev_name = self.cBTopic.currentText()
        self.prev_period = self.cBPeriod.currentIndex()

# MQTT logger callback
def mqtt_log_cb(client, userdata, level, buf):
    if debug:
        print('log:', buf)

# MQTT message event callback
def mqtt_message_cb(cli, userdata, message):
    message_s = str(message.payload.decode('utf-8'))

    if debug:
        print(message.topic, message.qos, message.retain, message_s)

    # pick out topic name, different for 'sensor' and 'stat' messages
    a = message.topic.split('/')

    if a[-3] == 'sensor':
        new_data['name'] = a[-2]
    elif a[-2] == 'stat':
        new_data['name'] = a[-1]
    else:
        print(message.topic + ': unable to handle topic')
        exit(1)

    # if we haven't seen this topic before, create a new Topic class instance
    if new_data['name'] not in topics:
        topics[new_data['name']] = Topic(new_data['name'])
        topics[new_data['name']].set_new(True)

    # update description in the matching Topic, and we're done
    if a[-1] == 'description':
        new_data.pop('seconds', None)
        new_data.pop('value', None)
        topics[new_data['name']].set_description(message_s)
        return

    # periods per Topic is not implemented, and we're done
    if a[-1] == 'period':
        new_data.pop('seconds', None)
        new_data.pop('value', None)
        return

    if a[-1] == 'microseconds':
        return      # ignore for now

    # accumulate seconds value in new_data
    if a[-1] == 'seconds':
        new_data.pop('value', None)
        new_data['seconds'] = int(message_s)
        return

    ### Data value messages follow
    # They will accumulate in new_data and then be added to the q
    # so that the MainWindow periodic update will add them to the
    # matching Topic's data series and graph it

    # 'sensor' messages are expected to have a 'data' entry here,
    # whereas 'stat' messages are their own data entry.
    if a[-3] == 'sensor' and a[-1] != 'data':
        print(message.topic + ': unable to handle data topic')
        exit(1)

    if new_data['seconds'] < int(gettime()) / 10:
        return      # ignore samples from 1970

    if message_s == 'nan' or message_s == 'inf' or message_s == '-inf':
        return      # ignore nan, inf, -inf

    # update new_data with the value
    new_data['value'] = float(message_s)

    if debug:
        print('q add:', new_data)

    # MainWindow periodic update is a different thread; it will find
    # the data sitting in the q fifo where it can process it by
    # updating the matching topic.
    # Strangely we seem to need to make a copy of new_data to append
    # to avoid values being erased from the q entry.
    q.append(new_data.copy())


# Main function, parse args, establish mqtt and graphics objects and threads
def main():
    global save_data_log
    global load_fname
    global save_fname

    parser = ArgumentParser(description='graph an mqtt topic.')
    parser.add_argument('--topic', default=topic_default, help='base topic for graph data')
    parser.add_argument('--host', default=host_default, help='mqtt server host')
    parser.add_argument('--log', dest='log', action='store_true', help='save data to log')
    parser.add_argument('--no-log', dest='log', action='store_false', help='save data to log')
    parser.set_defaults(log=log_default)

    args = parser.parse_args()

    save_data_log = args.log

    # could give it a persistent client id here as first argument
    client = MQTTClient()
    client.on_log = mqtt_log_cb
    client.on_message = mqtt_message_cb

    client.connect(str(args.host))

    client.loop_start()

    a = args.topic.split('/')

    title = ' '.join(a[1:3])
    load_fname = '_'.join(a[1:3]) + ".log"
    save_fname = '_'.join(a[1:3]) + ".log"

    app = QApplication(argv)
    w = MainWindow(title)
    w.show()

    client.subscribe(args.topic)

    code = app.exec_()

    client.loop_stop()

    exit(code)

if __name__ == '__main__':
    main()
