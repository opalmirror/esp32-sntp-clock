# SNTP Clock

This is a FreeRTOS project for Espressif's ESP32-DevkitC-V4.
It implements an SNTP clock application.
It uses the LwIP SNTP module to obtain time from an NTP server on the LAN.

## SNTP Operation

The application schedules a periodic SNTP query to the server.
The first adjustment is a time step using settimeofday.
Subsequent adjustments smoothly adjust the time using adjtime.
The period starts off short (64 seconds), and then after several successful cycles doubles, and this continues until it reaches a maximum period (2048 seconds).

On each cycle, when an SNTP response is received from the server, the application accumulates the local clock rate error value (difference between the local clock value and the SNTP time) in an array of fixed maximum size, expressed in parts per million (milliseconds per second).
It performs a linear regression on the time series data to determine a local clock rate error.
It then uses adjtime to correct the clock rate error once every second.
This keeps the time more accurate even when the SNTP server cannot be reached, and keeps the periods when adjtime is being applied short.

Loss of WiFi connection for a long time or SNTP server can result in the application changing the period back to 64 seconds.

## MQTT

The application posts messages to an MQTT broker (server).
Included is a Python 3 graphing application called client_plot.py which may be used to visualize the trend data.

MQTT data include sensor values (temperature and relative humidity) from the two sensors, and SNTP health, status, and event counters.

## Console

The Debug serial connection is used as a console; it supports a few commands like:

 * flash - show settings and other flash contents.
 * reset - soft reset the CPU.
 * save - commit settings changes from 'set' to the flash storage.
 * set - change a configuration setting. Use 'save' to make permanent.
 * status - show health and event counter data
 * tasks - show FreeRTOS task status including idle time and stack usage

# Sensors

Temperature sensors, HTU21DF chips, are connected to the two I2C buses of the the ESP32.

# Display

A display driver chip, HT16K33, is connected to the first I2C bus of the ESP32.
The chip drives a 1.2" clock display part which has four 7-segment LEDs, a leading and middle colon symbol, and a dot between the 3rd and 4th digits near the top.

# Buttons

Three button devices are attached to GPIO pins on the ESP32.
The first button cycles through available display brightness levels.

## AUTHOR

James Perkins, james@loowit.net, opalmirror@gmail.com, June 2022

## COPYRIGHT

Portions of this code were derived from examples and are Public Domain or CC0,
especially main/clock_main.c and main/wifi_connect.c .

Portions of this code are GPLv3, specifically client_plot.py

The remainer of the code is Copyright 2018 James Perkins.
License BSD-3-Clause.
See file COPYING in this software distribution for for details.
