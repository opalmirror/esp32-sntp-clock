/* non-volatile storage keys */

#define FLASH_WIFI_HOSTNAME     "wifi.hostname"
#define FLASH_WIFI_SSID         "wifi.ssid"
#define FLASH_WIFI_PASSWORD     "wifi.password"
#define FLASH_SENSORS_BROKER    "sensors.broker"
#define FLASH_SENSORS_SITERM    "sensors.siterm"
#define FLASH_SENSORS_PERIOD    "sensors.period"
#define FLASH_SNTP_SERVER       "sntp.server"
#define FLASH_SNTP_INTERVAL_MIN "sntp.int_min"
#define FLASH_SNTP_INTERVAL_MAX "sntp.int_max"
#define FLASH_SNTP_ERRPPM_MAX   "sntp.errppm_max"
#define FLASH_SNTP_STDERR_MAX   "sntp.stderr_max"
#define FLASH_SNTP_PPB          "sntp.ppb"
#define FLASH_SNTP_PPB_TIME     "sntp.ppb_time"
#define FLASH_DISPLAY_FLAGS     "display.flags"
#define FLASH_DISPLAY_BRIGHT    "display.bright"
#define FLASH_DISPLAY_TZ_OFF    "display.tz_off"
#define FLASH_DISPLAY_TZ        "display.tz"

enum flash_e
{
    FLASH_WIFI_HOSTNAME_LEN = 20,   /* Note: UNIX hostname is 1-63 characters */
    FLASH_WIFI_SSID_LEN = 20,   /* Note: IEEE 802.11-2012 says SSID is 32 bytes of any value */
    FLASH_WIFI_PASSWORD_LEN = 20,
    FLASH_SENSOR_BROKER_LEN = 40,
    FLASH_SENSOR_SITERM_LEN = 20,
    FLASH_SNTP_SERVER_LEN = 40,
    FLASH_TZ_LEN = 30,
    FLASH_MAX_STR_SIZE = 40,    /* no string entries will be larger than this */
};

struct flash_data_s
{
    struct
    {
        char hostname[FLASH_WIFI_HOSTNAME_LEN]; /* ex: c-office */
        char ssid[FLASH_WIFI_SSID_LEN]; /* ssid string */
        char password[FLASH_WIFI_PASSWORD_LEN]; /* password string */
    } wifi;

    struct
    {
        char mqtt_broker[FLASH_SENSOR_BROKER_LEN];  /* immediately follows mqtt:// */
        char mqtt_site_room[FLASH_SENSOR_SITERM_LEN];   /* follows broker, ex: hopewood/office */
        uint16_t period_s;      /* sensors period in seconds */
    } sensors;

    struct
    {
        char server[FLASH_SNTP_SERVER_LEN]; /* hostname or IP address */

        uint16_t interval_min_s;    /* min SNTP sync interval */
        uint16_t interval_max_s;    /* max SNTP sync interval */

        uint16_t clock_error_ppm_max;   /* max expected clock ppm */
        uint16_t clock_error_stderr_max;    /* max expected stderr of clock ppm */

        int32_t ppb;            /* last ppb measurement */
        time_t ppb_time;        /* last ppb measurement */
    } sntp;

    struct
    {
        uint16_t flags;         /* see enum display_flags */
        uint8_t brightness;     /* display brightness index */
        int16_t tz_off_min;     /* timezone offset, ex: 60*8=2400 */
        char tz[FLASH_TZ_LEN];  /* timezone string, ex: UTC */
    } display;
};

typedef struct flash_data_s flash_data;

void flash_init(void);
void flash_read(flash_data * p);
void flash_write(const flash_data * p);
void flash_show(const flash_data * p);
