/* SNTP LED clock application, HT16K33 chip and 4x7-segment display functions

*/

#include "freertos/FreeRTOS.h"
#include "esp_event.h"
#include "clock_i2c.h"

#include "defaults.h"
#include "ht16k33.h"
#include "ticks.h"

/**************************************
 * private definitions
 **************************************/

enum
{
    HT16K33_DEVICE_ADDRESS = 0x70,
};

/* *INDENT-OFF* */
enum HT16K33_registers
{
    HT16K33_POS_0          = 0x00,  /* right-most, least significant digit */
    HT16K33_POS_1          = 0x02,
    HT16K33_POS_2          = 0x04,  /* colons and ampm */
    HT16K33_POS_3          = 0x06,
    HT16K33_POS_4          = 0x08,  /* left-most, most significant digit */

    HT16K33_STANDBY        = 0x20,
    HT16K33_ON             = 0x21,

    HT16K33_DISPLAYOFF     = 0x80,
    HT16K33_DISPLAYON      = 0x81,
    HT16K33_BLINKON2HZ     = 0x83,
    HT16K33_BLINKON1HZ     = 0x85,
    HT16K33_BLINKON0_5HZ   = 0x87,

    HT16K33_BRIGHTNESS_0   = 0xE0,
    HT16K33_BRIGHTNESS_1   = 0xE1,
    HT16K33_BRIGHTNESS_2   = 0xE2,
    HT16K33_BRIGHTNESS_3   = 0xE3,
    HT16K33_BRIGHTNESS_4   = 0xE4,
    HT16K33_BRIGHTNESS_5   = 0xE5,
    HT16K33_BRIGHTNESS_6   = 0xE6,
    HT16K33_BRIGHTNESS_7   = 0xE7,
    HT16K33_BRIGHTNESS_8   = 0xE8,
    HT16K33_BRIGHTNESS_9   = 0xE9,
    HT16K33_BRIGHTNESS_A   = 0xEA,
    HT16K33_BRIGHTNESS_B   = 0xEB,
    HT16K33_BRIGHTNESS_C   = 0xEC,
    HT16K33_BRIGHTNESS_D   = 0xED,
    HT16K33_BRIGHTNESS_E   = 0xEE,
    HT16K33_BRIGHTNESS_F   = 0xEF,
};
/* *INDENT-ON* */

//      ==A==
//    |       |       A = 0x01
//    F       B       B = 0x02
//    |       |       C = 0x04
//      ==G==         D = 0x08
//    |       |       E = 0x10
//    E       C       F = 0x20
//    |       |       G = 0x40
//      ==D==   DP   DP = 0x80

enum
{
    S_ = 0x00,
    SA = 0x01,
    SB = 0x02,
    SC = 0x04,
    SD = 0x08,
    SE = 0x10,
    SF = 0x20,
    SG = 0x40,
    SDP = 0x80,
    HT16K33_SPACE = 0x10,
    HT16K33_MINUS = 0x11,
};

// 4:2                4:4
//     [0] [2] 4:1 [6] [8]
// 4:3

/**************************************
 * private data
 **************************************/

static const uint8_t ht16k33_encode[] = {
    SA | SB | SC | SD | SE | SF | S_,   // 0
    S_ | SB | SC | S_ | S_ | S_ | S_,   // 1
    SA | SB | S_ | SD | SE | S_ | SG,   // 2
    SA | SB | SC | SD | S_ | S_ | SG,   // 3
    S_ | SB | SC | S_ | S_ | SF | SG,   // 4
    SA | S_ | SC | SD | S_ | SF | SG,   // 5
    SA | S_ | SC | SD | SE | SF | SG,   // 6
    SA | SB | SC | S_ | S_ | S_ | S_,   // 7
    SA | SB | SC | SD | SE | SF | SG,   // 8
    SA | SB | SC | SD | S_ | SF | SG,   // 9
    SA | SB | SC | S_ | SE | SF | SG,   // A
    S_ | S_ | SC | SD | SE | SF | SG,   // b
    SA | S_ | S_ | SD | SE | SF | S_,   // C
    S_ | SB | SC | SD | SE | S_ | SG,   // d
    SA | S_ | S_ | SD | SE | SF | SG,   // E
    SA | S_ | S_ | S_ | SE | SF | SG,   // F
    S_ | S_ | S_ | S_ | S_ | S_ | S_,   // space
    S_ | S_ | S_ | S_ | S_ | S_ | SG,   // minus
};

static const uint8_t encode_char[][2] = {
    {'\0', S_ | S_ | S_ | S_ | S_ | S_ | S_},
    {' ', S_ | S_ | S_ | S_ | S_ | S_ | S_},
    {'0', SA | SB | SC | SD | SE | SF | S_},
    {'1', S_ | SB | SC | S_ | S_ | S_ | S_},
    {'2', SA | SB | S_ | SD | SE | S_ | SG},
    {'3', SA | SB | SC | SD | S_ | S_ | SG},
    {'4', S_ | SB | SC | S_ | S_ | SF | SG},
    {'5', SA | S_ | SC | SD | S_ | SF | SG},
    {'6', SA | S_ | SC | SD | SE | SF | SG},
    {'7', SA | SB | SC | S_ | S_ | S_ | S_},
    {'8', SA | SB | SC | SD | SE | SF | SG},
    {'9', SA | SB | SC | SD | S_ | SF | SG},
    {'=', S_ | S_ | S_ | SD | S_ | S_ | SG},
    {'?', SA | SB | S_ | S_ | SE | S_ | SG},
    {'A', SA | SB | SC | S_ | SE | SF | SG},
    {'b', S_ | S_ | SC | SD | SE | SF | SG},
    {'C', SA | S_ | S_ | SD | SE | SF | S_},
    {'c', S_ | S_ | S_ | SD | SE | S_ | SG},
    {'d', S_ | SB | SC | SD | SE | S_ | SG},
    {'E', SA | S_ | S_ | SD | SE | SF | SG},
    {'F', SA | S_ | S_ | S_ | SE | SF | SG},
    {'G', SA | S_ | SC | SD | SE | SF | S_},
    {'H', S_ | SB | SC | S_ | SE | SF | SG},
    {'h', S_ | S_ | SC | S_ | SE | SF | SG},
    {'I', S_ | S_ | S_ | S_ | SE | SF | S_},
    {'i', S_ | S_ | SC | S_ | S_ | S_ | S_},
    {'J', S_ | SB | SC | SD | S_ | S_ | S_},
    {'L', S_ | S_ | S_ | SD | SE | SF | S_},
    {'n', S_ | S_ | SC | S_ | SE | S_ | SG},
    {'O', SA | SB | SC | SD | SE | SF | S_},
    {'o', S_ | S_ | SC | SD | SE | S_ | SG},
    {'P', SA | SB | S_ | S_ | SE | SF | SG},
    {'q', SA | SB | SC | S_ | S_ | SF | SG},
    {'r', S_ | S_ | S_ | S_ | SE | S_ | SG},
    {'S', SA | S_ | SC | SD | S_ | SF | SG},
    {'t', S_ | S_ | S_ | SD | SE | SF | SG},
    {'U', S_ | SB | SC | SD | SE | SF | S_},
    {'u', S_ | S_ | SC | SD | SE | S_ | S_},
    {'y', S_ | SB | SC | SD | S_ | SF | SG},
};

#define num_encode_chars (sizeof(encode_char) / sizeof(*encode_char))

static const uint8_t brightnesses[] = { 0, 1, 2, 4, 8, 0xf };

static const unsigned int num_brightnesses =
    sizeof(brightnesses) / sizeof(*brightnesses);

static uint16_t flags = HAS_DISPLAY;

static uint8_t brightness_index = DISPLAY_BRIGHTNESS_DEF;
static uint8_t last_brightness_index = DISPLAY_BRIGHTNESS_DEF;

/**************************************
 * private functions
 **************************************/

static uint8_t ht16k33_encode_char(char c)
{
    int exact = -1;
    int lower = -1;
    int upper = -1;

    for (int n = 0; n < num_encode_chars; n++)
    {
        if (encode_char[n][0] == c)
        {
            exact = n;
            break;
        }
        else if (tolower(encode_char[n][0]) == c)
        {
            lower = n;
        }
        else if (toupper(encode_char[n][0]) == c)
        {
            upper = n;
        }
    }

    if (exact == -1)
    {
        exact = (lower != -1) ? lower : (upper != -1) ? upper : 0;
    }

    return encode_char[exact][1];
}

static int ht16k33_brightness_set(const i2c_handle h)
{
    const uint8_t chip_addr = HT16K33_DEVICE_ADDRESS;
    const uint8_t reg = HT16K33_BRIGHTNESS_0 + brightnesses[brightness_index];

    if (i2c_addr_write(h, chip_addr, reg, NULL, 0) != 0)
    {
        return -1;
    }

    return 0;
}

/**************************************
 * public functions
 **************************************/

int ht16k33_init(const i2c_handle h, uint16_t f, uint8_t brightness)
{
    flags = f;

    if (!(flags & HAS_DISPLAY))
    {
        return 0;
    }

    brightness_index = brightness;
    last_brightness_index = brightness;

    const uint8_t chip_addr = HT16K33_DEVICE_ADDRESS;
    /* *INDENT-OFF* */
    const uint8_t init_reg[] =
        { HT16K33_STANDBY, HT16K33_ON, HT16K33_DISPLAYON };
    /* *INDENT-ON* */
    int status = -1;

    for (int i = 0; i < sizeof(init_reg); i++)
    {
        if (i2c_addr_write(h, chip_addr, init_reg[i], NULL, 0) != 0)
        {
            goto done;
        }
    }

    if (ht16k33_brightness_set(h) != 0)
    {
        goto done;
    }

    ht16k33_display_number(h, 8888);
    ht16k33_display_dots(h, ALL_DOTS);
    vTaskDelay(ms_to_ticks(1000));

    ht16k33_display_number(h, 0);
    ht16k33_display_dots(h, 0);
    vTaskDelay(ms_to_ticks(1000));

    status = 0;

 done:
    return status;
}

void ht16k33_brightness_next(void)
{
    brightness_index++;
    brightness_index %= num_brightnesses;

    /* TODO: queue a request to a LED handler task */
}

unsigned int ht16k33_brightness_get(void)
{
    return brightness_index;
}

int ht16k33_display_string(const i2c_handle h, const char *s)
{
    if (!(flags & HAS_DISPLAY))
    {
        return 0;
    }

    const uint8_t chip_addr = HT16K33_DEVICE_ADDRESS;
    int status = -1;
    const uint8_t position[] =
        { HT16K33_POS_2, HT16K33_POS_1, HT16K33_POS_3, HT16K33_POS_4 };

    for (int i = 0; i < 4; i++, (*s && s++))
    {
        uint8_t reg = position[i];
        uint8_t val = ht16k33_encode_char(*s);
        uint8_t zero = 0;

        if (i2c_addr_write(h, chip_addr, reg + 1, &zero, 1) != 0)
        {
            goto done;
        }

        if (i2c_addr_write(h, chip_addr, reg, &val, 1) != 0)
        {
            goto done;
        }
    }

    status = 0;
 done:
    return status;
}

int ht16k33_display_number(const i2c_handle h, int d)
{
    if (!(flags & HAS_DISPLAY))
    {
        return 0;
    }

    const uint8_t chip_addr = HT16K33_DEVICE_ADDRESS;
    int status = -1;
    const uint8_t position[] =
        { HT16K33_POS_4, HT16K33_POS_3, HT16K33_POS_1, HT16K33_POS_2 };

    for (int i = 0; i < 4; i++, d /= 10)
    {
        uint8_t reg = position[i];
        uint8_t val = ht16k33_encode[d % 10];
        uint8_t zero = 0;

        if (i2c_addr_write(h, chip_addr, reg + 1, &zero, 1) != 0)
        {
            goto done;
        }

        if (i2c_addr_write(h, chip_addr, reg, &val, 1) != 0)
        {
            goto done;
        }
    }

    status = 0;
 done:
    return status;
}

int ht16k33_display_dots(const i2c_handle h, uint8_t d)
{
    if (!(flags & HAS_DISPLAY))
    {
        return 0;
    }

    const uint8_t chip_addr = HT16K33_DEVICE_ADDRESS;
    int status = -1;

    uint8_t reg = HT16K33_POS_0;
    uint8_t zero = 0;

    if (i2c_addr_write(h, chip_addr, reg + 1, &zero, 1) != 0)
    {
        goto done;
    }

    if (i2c_addr_write(h, chip_addr, reg, &d, 1) != 0)
    {
        goto done;
    }

    if (brightness_index != last_brightness_index)
    {
        if (ht16k33_brightness_set(h) != 0)
        {
            goto done;
        }

        last_brightness_index = brightness_index;
    }

    status = 0;
 done:
    return status;
}
