/* SNTP LED clock application, HT16K33 chip and 4x7-segment display API

*/

enum display_flags
{
    HAS_DISPLAY = 0x01,
    DISPLAY_TEMP_C = 0x02,
    DISPLAY_TEMP_F = 0x04,
    DISPLAY_RH = 0x08,
    DISPLAY_AMPM = 0x10,
};

enum ht16k33_dots
{
    CENTER_COLON = 0x02,
    TOP_LEFT_DOT = 0x04,
    BOTTOM_LEFT_DOT = 0x08,
    TOP_RIGHT_DOT = 0x10,
    ALL_DOTS = 0x1E,
};

int ht16k33_init(const i2c_handle h, uint16_t flags, uint8_t brightness)
    __attribute__((nonnull(1)));

void ht16k33_brightness_next(void);

unsigned int ht16k33_brightness_get(void);

int ht16k33_display_string(const i2c_handle h, const char *s)
    __attribute__((nonnull(1, 2)));

int ht16k33_display_number(const i2c_handle h, int d)
    __attribute__((nonnull(1)));

/* mask of enum ht16k33_dots */
int ht16k33_display_dots(const i2c_handle h, uint8_t dots)
    __attribute__((nonnull(1)));
