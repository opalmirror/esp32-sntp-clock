/* I2C for SNTP LED clock application, common i2c API

*/

typedef struct
{
    uint8_t gpio_sda;           /*!< chip GPIO to use for I2C serial data */
    uint8_t gpio_scl;           /*!< chip GPIO to use for I2C serial clock */
    uint32_t frequency;         /*!< I2C bus bit clock frequency */
    uint8_t port;               /*!< I2C port device to use */
} clock_i2c_t;

typedef const clock_i2c_t *i2c_handle;  /* descriptor/handle for open i2c port */

i2c_handle i2c_start(const clock_i2c_t * p) __attribute__((nonnull(1)));

void i2c_stop(i2c_handle h) __attribute__((nonnull(1)));

int i2c_addr_write(const i2c_handle h, uint8_t chip_addr, uint8_t reg,
    const uint8_t * data, int len) __attribute__((nonnull(1)));

int i2c_addr_read(const i2c_handle h, uint8_t chip_addr, uint8_t * data,
    int len) __attribute__((nonnull(1, 3)));

int i2c_addr_write_read(const i2c_handle h, uint8_t chip_addr, uint8_t reg,
    uint8_t * data, int read_len) __attribute__((nonnull(1, 4)));
