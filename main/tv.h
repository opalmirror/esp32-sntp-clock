/* SNTP LED clock application, timeval API

*/

/* convert us to s, returning result */
int64_t us_to_s(int64_t us);

/* convert s to us, returning result */
int64_t s_to_us(int64_t s);

/* convert timeval to us, returning result */
int64_t tv_to_us(const struct timeval *tv) __attribute__((nonnull));

/* convert us to timeval, store result in tv */
void us_to_tv(int64_t us, struct timeval *tv) __attribute__((nonnull));

/* convert timeval to double, returning result */
double tv_to_double(const struct timeval *tv) __attribute__((nonnull));

/* convert double to timeval, store in tv */
void double_to_tv(double d, struct timeval *tv) __attribute__((nonnull));

/* store time zero in tv */
void tv_zero(struct timeval *tv) __attribute__((nonnull));

/* compare timevals t and u, and return -1 if t < u, 0 if t == u, 1 if t > u */
int tv_cmp(const struct timeval *t, const struct timeval *u)
    __attribute__((nonnull));

/* change sign of timeval t, store in result */
void tv_negate(const struct timeval *t, struct timeval *result)
    __attribute__((nonnull));

/* add timevals t and u, store in result */
void tv_add(const struct timeval *t, const struct timeval *u,
    struct timeval *result) __attribute__((nonnull));

/* subtract timeval u from t, store in result */
void tv_subtract(const struct timeval *t, const struct timeval *u,
    struct timeval *result) __attribute__((nonnull));
