/* SNTP LED clock application, button functions

*/

/*
#define DEBUG 1
*/

/* FreeRTOS and EspressIF includes */
#include "freertos/FreeRTOS.h"
#include "freertos/timers.h"
#include "esp_event.h"
#include "driver/gpio.h"

#ifdef DEBUG
#include "esp_log.h"
#endif

/* application includes */
#include "button.h"
#include "ticks.h"

/**************************************
 * private data
 **************************************/

#ifdef DEBUG
static const char *TAG = "button";
#endif

typedef struct
{
    uint8_t gpio;               /* copy needed in IRAM */
    const button_t *config;
    xQueueHandle button_q;
    TaskHandle_t task_h;
} button_ctl_t;

enum
{
    ESP_INTR_FLAG_DEFAULT = 0,
    BUTTON_DEBOUNCE_PERIOD_MS = 50,
    BUTTON_PRESSED = 0,         /* button press is active low */
    BUTTON_NOT_PRESSED = 1,     /* button press is inactive high */
};

/**************************************
 * private functions
 **************************************/

static void IRAM_ATTR button_isr_handler(void *arg)
{
    button_ctl_t *p = (button_ctl_t *) arg;

    xQueueSendFromISR(p->button_q, p, NULL);
}

static void button_timeout(TimerHandle_t timer __attribute__((unused)))
{
}

static void button_task(void *arg)
{
    const button_ctl_t *p = (button_ctl_t *) arg;

    bool was_pressed = false;
    TimerHandle_t timer;
    unsigned int ticks_debounce = ms_to_ticks(BUTTON_DEBOUNCE_PERIOD_MS);

    char name[11];
    snprintf(name, sizeof(name), "button%u", p->config->n);

    timer = xTimerCreate(name, ticks_debounce, pdFALSE, NULL, button_timeout);

    for (;;)
    {
        button_ctl_t *junk;
        const unsigned int ticks_max = portMAX_DELAY;
        unsigned int ticks = was_pressed ? ticks_debounce + 1 : ticks_max;

        if (xQueueReceive(p->button_q, &junk, ticks))
        {
            int val = gpio_get_level(p->config->gpio);
#if DEBUG
            ESP_LOGI(TAG, "GPIO[%d] intr, val: %d\n", p->config->gpio, val);
#endif

            if (val == BUTTON_PRESSED)
            {
                xTimerReset(timer, 0);
                was_pressed = true;
#if DEBUG
                ESP_LOGI(TAG, "button pressed, start timer");
#endif
            }

            continue;           /* nothing to do until later */
        }

        /* queue wait has expired */

        if (!was_pressed)
        {
#if DEBUG
            ESP_LOGI(TAG, "expiry");
#endif
            continue;           /* no button press outstanding */
        }

        /* button had been pressed */

        if (xTimerIsTimerActive(timer) == pdTRUE)
        {
#if DEBUG
            ESP_LOGI(TAG, "not yet");
#endif
            continue;           /* wait a little longer */
        }

        /* timer is no longer active */

        if (gpio_get_level(p->config->gpio) == BUTTON_NOT_PRESSED)
        {
            was_pressed = false;    /* look for next button press */
#if DEBUG
            ESP_LOGI(TAG, "false alert");
#endif
            continue;           /* button is no longer pressed */
        }

        /* button is still pressed */

#if DEBUG
        ESP_LOGI(TAG, "callback");
#endif

        p->config->callback_fcn(p->config);

        was_pressed = false;    /* look for next button press */
    }

    vQueueDelete(p->button_q);
    vTaskDelete(NULL);
}

/**************************************
 * public functions
 **************************************/

int button_get_level(const button_t * p)
{
    int val = gpio_get_level(p->gpio);

    return val;
}

void button_start(const button_t * p)
{
    gpio_config_t io_conf = {
        .intr_type = GPIO_INTR_NEGEDGE,
        .mode = GPIO_MODE_INPUT,
        .pin_bit_mask = (1ULL << p->gpio),
        .pull_down_en = 0,
        .pull_up_en = 1,
    };

    button_ctl_t *ctl = malloc(sizeof(*ctl));

    ctl->config = p;

    gpio_config(&io_conf);

    ctl->button_q = xQueueCreate(10, sizeof(uint32_t));

    char name[11];
    snprintf(name, sizeof(name), "button%u", p->n);

    const unsigned int STACK_SIZE = 2 * 1024;
    xTaskCreate(button_task, name, STACK_SIZE, ctl, tskIDLE_PRIORITY + 2,
        &ctl->task_h);

    gpio_install_isr_service(ESP_INTR_FLAG_DEFAULT);
    gpio_isr_handler_add(p->gpio, button_isr_handler, ctl);
}

void button_stop(const button_t * p)
{
    /* TODO */
}
