/* SNTP LED clock application, statistics functions

*/

/* C library includes */
#include <assert.h>
#include <math.h>

/* FreeRTOS includes */
#include "freertos/FreeRTOS.h"
#include "esp_event.h"

/* Application includes */
#include "stats.h"

struct _stats_s
{
    uint8_t n;                  // number of valid data (limited to max)
    uint8_t max;                // max number of valid data
    uint8_t index;              // array index of next data point to write

    double *px;                 // data x values
    double *py;                 // data y values
    double *pw;                 // data point weight values

    bool *pvalid;               // data are valid
    double *pdy;                // residual of y vs. linear regression line
    double *pr;                 // residual sort array
    uint8_t nvalid;             // how many points are valid
    double xmeanv;              // mean of valid x
    double ymeanv;              // mean of valid y

    double slope;               // linear regression slope
    double intercept;           // linear regression y intercept

    double y_stddev;            // standard deviation of y data

    double xmin;                // smallest x value
    double xmax;                // largest x value
};

/**************************************
 * private functions
 **************************************/

/* add point x, y to statistics, incrementing n and index modulo max */

static void add_xy(stats_t p, double x, double y, double w)
{
    p->px[p->index] = x;
    p->py[p->index] = y;
    p->pw[p->index] = w;

    /* bump up n but not to exceed p->max */
    p->n++;
    p->n = p->n > p->max ? p->max : p->n;

    /* advance to next index with wraparound */
    p->index++;
    p->index %= p->max;
}

/*
 * determine x and y mean of valid points, range of all x, and count of
 * valid points
 */

static void get_xmeanv_ymeanv_xmin_xmax_nvalid(stats_t p)
{
    double sumx = 0.0;
    double sumy = 0.0;
    double sumw = 0.0;

    p->nvalid = 0;
    p->ymeanv = 0.0;
    p->xmeanv = 0.0;

    p->xmin = p->px[0];
    p->xmax = p->px[0];

    for (uint8_t i = 0; i < p->n; i++)
    {
        double x = p->px[i];

        p->xmin = p->xmin <= x ? p->xmin : x;
        p->xmax = p->xmax >= x ? p->xmax : x;

        if (!p->pvalid[i])
        {
            continue;
        }

        p->nvalid++;

        sumx += p->pw[i] * p->px[i];
        sumy += p->pw[i] * p->py[i];
        sumw += p->pw[i];
    }

    p->xmeanv = sumx / sumw;
    p->ymeanv = sumy / sumw;
}

/* determine standard deviation of all y */

static void get_y_stddev(stats_t p)
{
    double sdev = 0.0;
    double sumw = 0.0;

    for (uint8_t i = 0; i < p->n; i++)
    {
        double var = p->py[i] - p->ymeanv;

        sdev += p->pw[i] * var * var;
        sumw += p->pw[i];
    }

    p->y_stddev = sqrt(sdev / sumw);
}

/* get slope and intercept for valid points */

static void get_slope_intercept_valid(stats_t p)
{
    /* for one point, slope is zero and intercept is y[0] */

    if (p->nvalid == 1)
    {
        p->slope = 0.0;
        p->intercept = p->py[0];

        return;                 /* no more work to do */
    }

    /* for two or more points, calculate slope and intercept */

    double numerator = 0.0;
    double denominator = 0.0;
    double sumw = 0.0;

    for (uint8_t i = 0; i < p->n; i++)
    {
        if (!p->pvalid[i])
        {
            continue;
        }

        double dx = p->px[i] - p->xmeanv;
        double dy = p->py[i] - p->ymeanv;

        numerator += p->pw[i] * dx * dy;
        denominator += dx * dx;
        sumw += p->pw[i];
    }

    p->slope = numerator / (sumw * denominator);
    p->intercept = p->ymeanv - p->slope * p->xmeanv;
}

/* find and mark invalid points */

static void mark_invalid_data(stats_t p, uint8_t nMedianResiduals)
{

    /*
     * determine residuals of valid p->pdy[i], place in p->pdy[i] and copy
     * into p->pr[0..nvalid-1].
     */

    uint8_t nn = 0;

    for (uint8_t i = 0; i < p->n; i++)
    {
        if (!p->pvalid[i])
        {
            continue;
        }

        p->pdy[i] = fabs(p->py[i] - stats_y_at_x(p, p->px[i]));
        p->pr[nn++] = p->pdy[i];
    }

    /* insertion sort on p->pr[] */

    for (uint8_t i = 1; i < p->nvalid; i++)
    {
        int j;
        double key = p->pr[i];

        for (j = i - 1; j >= 0 && p->pr[j] > key; j--)
        {
            p->pr[j + 1] = p->pr[j];
        }

        p->pr[j + 1] = key;
    }

    /* median y value from of p->pr[] */

    double dymedian = p->pr[p->nvalid / 2];

    /*
     * invalidate any elements in p->pdy[] with residual more than n
     * times the median residual; update nvalid.
     */

    for (uint8_t i = 0; i < p->n; i++)
    {
        if (p->pvalid[i] && p->pdy[i] > nMedianResiduals * dymedian)
        {
            p->pvalid[i] = false;
            p->nvalid--;
        }
    }
}

/**************************************
 * public functions
 **************************************/

stats_t stats_start(uint8_t max)
{
    stats_t p;

    assert(max > 0);

    p = malloc(sizeof(*p));
    p->n = 0;
    p->nvalid = 0;
    p->max = max;
    p->index = 0;
    p->px = malloc(max * sizeof(*p->px));
    p->py = malloc(max * sizeof(*p->py));
    p->pw = malloc(max * sizeof(*p->pw));
    p->pvalid = malloc(max * sizeof(*p->pvalid));
    p->pdy = malloc(max * sizeof(*p->pdy));
    p->pr = malloc(max * sizeof(*p->pr));
    p->slope = 0.0;
    p->intercept = 0.0;
    p->xmeanv = 0.0;
    p->ymeanv = 0.0;
    p->y_stddev = 0.0;
    p->xmin = 0.0;
    p->xmax = 0.0;

    return p;
}

void stats_stop(stats_t p)
{
    free(p->px);
    free(p->py);
    free(p->pw);
    free(p->pvalid);
    free(p->pdy);
    free(p->pr);
    free(p);
}

bool stats_add_data(stats_t p, double x, double y, double w)
{
    const uint8_t nReps = 2;
    const uint8_t nMedianResiduals = 5;
    uint8_t index = p->index;

    add_xy(p, x, y, w);

    get_y_stddev(p);

    /* set all data true initially */
    for (uint8_t i = 0; i < p->n; i++)
    {
        p->pvalid[i] = true;
    }

    p->nvalid = p->n;

    /*
     * Run linear regression.  On each pass calculate the residual for
     * each data point and find the median residual.  Invalidate points
     * with a residual more than than the median times nMedianResiduals.
     * Repeat linear regression with the valid points.
     */

    for (uint8_t rep = 0; rep < nReps; rep++)
    {
        get_xmeanv_ymeanv_xmin_xmax_nvalid(p);

        get_slope_intercept_valid(p);

        if (p->nvalid == 1)
        {
            return false;       /* no more work to do */
        }

        /* find and mark any invalid points before the next round of linear regression */

        if (rep < nReps - 1)
        {
            mark_invalid_data(p, nMedianResiduals);
        }
    }

    return p->pvalid[index];
}

uint8_t stats_n(const stats_t p)
{
    return p->n;
}

uint8_t stats_n_valid(const stats_t p)
{
    return p->nvalid;
}

double stats_y_stddev(const stats_t p)
{
    return p->y_stddev;
}

double stats_y_stderr(const stats_t p)
{
    return p->y_stddev / sqrt(p->n);
}

double stats_y_95pct_confidence_interval(const stats_t p)
{
    return 1.96 * stats_y_stderr(p);
}

double stats_slope(const stats_t p)
{
    return p->slope;
}

double stats_intercept(const stats_t p)
{
    return p->intercept;
}

double stats_y_at_x(const stats_t p, double x)
{
    return p->slope * x + p->intercept;
}

double stats_mean(const stats_t p)
{
    return stats_y_at_x(p, (p->xmax + p->xmin) / 2.0);
}
