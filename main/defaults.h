#define WIFI_HOSTNAME_DEF "c-mbr"   /* WiFi hostname */
#define WIFI_SSID_DEF "weavings"    /* WiFi SSID */
#define WIFI_PASSWORD_DEF "pelitra4197" /* WiFi password */

#define SENSORS_MQTT_BROKER_DEF "192.168.250.116"   /* MQTT broker hostname/IP address */
#define SENSORS_MQTT_SITE_ROOM_DEF "hopewood/mbr"   /* MQTT site and room */
#define SENSORS_PERIOD_S_DEF 60 /* Sensors sample period */

#define SNTP_SERVER_DEF "192.168.250.110"   /* SNTP server hostname/IP address */
#define SNTP_INTERVAL_MIN_S_DEF 64  /* SNTP minimum interval */
#define SNTP_INTERVAL_MAX_S_DEF 2048    /* SNTP maximum interval */
#define SNTP_CLOCK_ERROR_PPM_MAX_DEF 50.0   /* SNTP max ppm to rate adjust */
#define SNTP_CLOCK_ERROR_STDERR_MAX_DEF 50.0    /* SNTP max stderr to rate adjust */

#define DISPLAY_FLAGS_DEF HAS_DISPLAY   /* Shows Time on display */
#define DISPLAY_BRIGHTNESS_DEF 2    /* Brightness Index (0..N) */
#define DISPLAY_TZ_OFF_MIN_DEF 0    /* Minutes offset (-25 * 60, 25 * 60) */

#define DISPLAY_TZ_DEF "PST8PDT,M3.2.0/2,M11.1.0"   /* Timezone description */
