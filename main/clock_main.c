/* SNTP LED clock with temperature/humidity sensor

   Derived from FreeRTOS LwIP SNTP example.

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

/* C library */
#include <math.h>
#include <string.h>
#include <ctype.h>

/* FreeRTOS and EspressIF includes */
#include "freertos/FreeRTOS.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_console.h"
#include "esp_sleep.h"
#include "esp_sntp.h"
#include "nvs_flash.h"

/* application includes */
#include "button.h"
#include "clock_i2c.h"
#include "defaults.h"
#include "flash.h"
#include "ht16k33.h"
#include "mqtt_tcp.h"
#include "htu21df.h"
#include "stats.h"
#include "ticks.h"
#include "tv.h"
#include "wifi_connect.h"

/**************************************
 * General application stuff
 **************************************/

#ifndef CONFIG_SNTP_TIME_SYNC_METHOD_SMOOTH
#error "code expects CONFIG_SNTP_TIME_SYNC_METHOD_SMOOTH to be set"
#endif

static const char *TAG = "clock_main";

/*
 * Variable holding number of times ESP32 restarted since first boot.
 * It is placed into RTC memory using RTC_DATA_ATTR and
 * maintains its value when ESP32 wakes from deep sleep.
 */
RTC_DATA_ATTR static int boot_count = 0;

RTC_DATA_ATTR static int32_t rtc_ppb = 0;
RTC_DATA_ATTR static time_t rtc_ppb_time = 0;
RTC_DATA_ATTR static double rtc_ppb_elapsed = 0.0;

static flash_data nvdata;
static uint32_t flash_writes = 0;

static int sntp_count = 0;      /* # of sntp time cycles since sntp_start */
static time_t sntp_time_expected = 0;   /* time that next sntp response is expected */

static bool verbose = true;     /* log verbosely - true until health goes all good */
static bool all_good = false;   /* health of all good has been seen once */

static struct __attribute__((packed))
{
    int8_t wifi:1;              /* wifi access point is connected */
    int8_t time:1;              /* time has been set once */
    int8_t sntp:1;              /* sntp results have been received and are not overdue */
    int8_t mqtt:1;              /* mqtt server is connected */
} health;                       /* health of subsystems, 1 if healthy, 0 if not (yet) */

/* Event counting and last time recording */

enum counter_e
{
    C_WIFI_DC = 0,
    C_TIME_LARGE_NEG,
    C_TIME_LARGE_POS,
    C_SNTP_DC,
    C_MQTT_DC,
    C_I2C_ERROR,
    C_TEMP_OOR,
    C_RH_OOR,
    C_N_ENTRIES,                /* must be last */
};

const char *counter_desc[C_N_ENTRIES] = {
    "WiFi disconn",
    "STNP large neg",
    "STNP large pos",
    "SNTP disconn",
    "MQTT disconn",
    "I2C error",
    "Temp o/o/range",
    "RH o/o/range",
};

uint16_t counter_n[C_N_ENTRIES];    /* count of error */
time_t counter_t[C_N_ENTRIES];  /* last time of error */

void counter_inc_t(enum counter_e n, time_t t)
{
    counter_n[n]++;
    counter_t[n] = t;
}

void counter_inc(enum counter_e n)
{
    struct timespec now;

    clock_gettime(CLOCK_REALTIME, &now);
    counter_n[n]++;
    counter_t[n] = now.tv_sec;
}

/**************************************
 * Sensor data
 **************************************/

#define NUM_SENSORS 2           /* sensor 0 on bus 0, sensor 1 on bus 1 */

uint8_t num_sensors = NUM_SENSORS;
float sensor_rh[NUM_SENSORS];
float sensor_temp_c[NUM_SENSORS];

/**************************************
 * I2C configuration
 **************************************/

#define NUM_I2C_BUSES NUM_SENSORS

const clock_i2c_t i2c_config[NUM_I2C_BUSES] = {
    {
            .gpio_sda = 18,
            .gpio_scl = 19,
            .frequency = 10000,
            .port = 0,
        },
    {
            .gpio_sda = 14,
            .gpio_scl = 12,
            .frequency = 10000,
            .port = 1,
        }
};

i2c_handle i2c_h[NUM_I2C_BUSES];

/**************************************
 * button pressed action callback
 **************************************/

static void button0_pressed_cb(const void *arg);
static void button1_pressed_cb(const void *arg);
static void button2_pressed_cb(const void *arg);

#define NUM_BUTTONS 3

// *INDENT-OFF*
const button_t button[NUM_BUTTONS] =
{
    {
        .n = 0,
        .gpio = 5,
        .callback_fcn = button0_pressed_cb,
    },
    {
        .n = 1,
        .gpio = 17,
        .callback_fcn = button1_pressed_cb,
    },
    {
        .n = 2,
        .gpio = 16,
        .callback_fcn = button2_pressed_cb,
    }
};
// *INDENT-ON*

/* Note: callbacks happen in context of buttonN task */

void button0_pressed_cb(const void *arg)
{
    ht16k33_brightness_next();

    nvdata.display.brightness = ht16k33_brightness_get();

    flash_write(&nvdata);
    flash_writes++;
}

void button1_pressed_cb(const void *arg)
{
    nvdata.display.flags ^= DISPLAY_AMPM;

#if 0
    ESP_LOGI(TAG, "%s: display flags 0x%02x", __func__, nvdata.display.flags);
#endif

    flash_write(&nvdata);
    flash_writes++;
}

void button2_pressed_cb(const void *arg)
{
    /* TODO */
}

/**************************************
 * timeval helpers
 **************************************/

/* Return adjtime()'s remaining delta in units of seconds */
static double get_adjtime(void)
{
    struct timeval delta;

    adjtime(NULL, &delta);

    return tv_to_double(&delta);
}

void localtime_convert(char *buf, unsigned n, time_t t)
{
    struct tm timeinfo;

    localtime_r(&t, &timeinfo);
    strftime(buf, n, "%c %Z %z", &timeinfo);
}

/**************************************
 * Periodic clock tweak.
 *
 * Try to keep individual adjtime amounts low by using the estimated
 * rate error (ppm) to call adjtime periodically throughout the
 * SNTP period.
 *
 **************************************/

static struct
{
    bool enable;                // true to enable periodic clock tweak
    struct timeval last;        // time last tweak was initiated
    struct timeval start;       // time to begin tweaking clock
    double ppm;                 // rate at which to tweak clock
    double sum;                 // total tweaks made since start time
} tweak;

/* initialize the tweak structure */
void tweak_init(void)
{
    tweak.enable = false;
    tv_zero(&tweak.start);
    tv_zero(&tweak.last);
    tweak.ppm = 0.0;
    tweak.sum = 0.0;
}

/* start tweaking the clock, or update the tweak rate */
void tweak_start(const struct timeval *now, double ppm)
{
    tweak.enable = true;
    tweak.start = *now;
    tweak.ppm = ppm;
    tweak.sum = 0.0;
}

/* periodically adjtime to correct estimated rate error.  */
void tweak_clock_periodic(const struct timeval *now)
{
    if (!tweak.enable || tweak.ppm == 0.0 || tv_cmp(now, &tweak.start) < 0)
    {
        return;
    }

    if (get_adjtime() != 0.0)
    {
        return;                 /* adjtime in process */
    }

    struct timeval delta;

    if (tv_cmp(&tweak.last, &tweak.start) < 0)
    {
        tweak.last = tweak.start;
    }

    /*
     * Calculate desired sum thru the end of this second.
     */

    struct timeval end_of_sec;
    struct timeval since_start;

    end_of_sec.tv_sec = now->tv_sec + 1;
    end_of_sec.tv_usec = 0;

    tv_subtract(&end_of_sec, &tweak.start, &since_start);

    double target_us = tv_to_double(&since_start) * tweak.ppm;

    /* note: round time adjustment to nearest us */

    int64_t us = round(target_us - tweak.sum * 1e6);

    us_to_tv(us, &delta);

    tweak.last = *now;
    tweak.sum += us * 1e-6;

#if 0
    ESP_LOGI(TAG, " tweak: %9.6f, sum %.6f", us * 1e-6, tweak.sum);
#endif

    adjtime(&delta, NULL);
}

/******************************
 * SNTP implementation
 ******************************/

static stats_t ppms;            // statistics for SNTP error ppms

/*
 * Notifier called by sntp_sync_time() after it begins or ends activity.
 *
 * Uses the tweak structure to coordinate corrections made by periodic_update()
 */
static void time_sync_notification_cb(const struct timeval *tv,
    const struct timeval *tv_now, const struct timeval *delta)
{
    struct saved_state_s
    {
        struct timeval last_time;
        double sum_tweak;
        double sum_sntp;
        int converging_count;
        int nv_ppm_changed_count;
    };

    static struct saved_state_s saved_state = {
        .last_time = {.tv_sec = 0,.tv_usec = 0,},
        .sum_tweak = 0.0,
        .sum_sntp = 0.0,
        .converging_count = 0,
        .nv_ppm_changed_count = 0,
    };

    double sntp_error = saved_state.last_time.tv_sec == 0 ? 0.0 : tv_to_us(delta) * 1e-6;   /* sntp adjusted error */
    sntp_sync_status_t status = sntp_get_sync_status();
    const char *str =
        status == SNTP_SYNC_STATUS_RESET ? "reset" :
        status == SNTP_SYNC_STATUS_COMPLETED ? "completed" :
        status == SNTP_SYNC_STATUS_IN_PROGRESS ? "in progress" : "unknown";
    char t_buf[64];

    localtime_convert(t_buf, sizeof(t_buf), tv_now->tv_sec);

    char *taskName = pcTaskGetName(NULL);

    health.sntp = 1;

    if (verbose)
    {
        ESP_LOGI(TAG, "Time: %s SNTP %s ms=%03ld task=%s", t_buf, str,
            tv_now->tv_usec / 1000L, taskName);
    }

    double elapsed;

    {
        if (saved_state.last_time.tv_sec == 0)
        {
            elapsed = 0.0;
        }
        else
        {
            struct timeval elapsed_tv;

            tv_subtract(tv, &saved_state.last_time, &elapsed_tv);
            elapsed = tv_to_double(&elapsed_tv);
        }

        saved_state.last_time = *tv;
    }

    double error = sntp_error + tweak.sum;  /* total predicted + sntp adjusted error */
    double ppm =
        elapsed == 0.0 ? nvdata.sntp.ppb * 1e-3 : error * 1e6 / elapsed;

#if 0
    /* if ppm is very large, skip adding it */

    bool valid = false;

    if (ppm != 0.0 && fabs(ppm) < 10.0 * nvdata.sntp.clock_error_ppm_max)
    {
        valid = stats_add_data(ppms, tv_to_double(tv), ppm, elapsed);
    }
#else
    const bool valid = true;

    if (ppm != 0.0)
    {
        (void)stats_add_data(ppms, tv_to_double(tv), ppm, elapsed);
    }
#endif

    double predicted_ppm =
        elapsed == 0.0 ? nvdata.sntp.ppb * 1e-3 : tweak.sum * 1e6 / elapsed;

    if (verbose)
    {
        ESP_LOGI(TAG,
            "   predicted ppm %9.3f from tweak %9.6f, sntp %9.6f, elapsed %.3f",
            predicted_ppm, tweak.sum, sntp_error, elapsed);
    }

    saved_state.sum_tweak += tweak.sum;
    saved_state.sum_sntp += valid ? sntp_error : 0.0;

    if (verbose)
    {
        ESP_LOGI(TAG,
            "                           accum   %12.6f    %12.6f",
            saved_state.sum_tweak, saved_state.sum_sntp);
    }

    /* consider longer interval */
    uint32_t interval = sntp_get_sync_interval();
    uint32_t next_interval = interval;

    saved_state.converging_count += sntp_count <= 4 ? 0 : 1;

    if (saved_state.converging_count >= 4 &&
        interval < nvdata.sntp.interval_max_s * 1000)
    {
        saved_state.converging_count = 0;
        next_interval *= 2;
        sntp_set_sync_interval(next_interval);
    }

    sntp_time_expected = tv->tv_sec + next_interval / 1000;

    bool tweak_use_nv_ppm = nvdata.sntp.ppb != 0 &&
        stats_n(ppms) < 0.67 * nStats;

    bool tweak_use_stat_ppm = !tweak_use_nv_ppm && stats_n(ppms) >= 4 &&
        fabs(stats_mean(ppms)) < nvdata.sntp.clock_error_ppm_max &&
        stats_y_stderr(ppms) < nvdata.sntp.clock_error_stderr_max;

    /*
     * inform update loop of need to adjtime small amounts
     * use last value or latest if it is in acceptable range (tweak_use_stat_ppm is true)
     */

    double tweak_ppm = tweak.ppm;

    bool incr_nv_ppm_changed_count = false;

    if (tweak_use_nv_ppm)
    {
        tweak_ppm = nvdata.sntp.ppb * 1e-3;
    }
    else if (tweak_use_stat_ppm)
    {
        tweak_ppm = stats_mean(ppms);

        /* remember last ppm time and value in case of deep sleep */
        rtc_ppb_time = tv->tv_sec;
        rtc_ppb_elapsed = elapsed;
        rtc_ppb = round(stats_mean(ppms) * 1e3);

        /*
         * determine if stats ppm is confidently and persistently
         * different from nvdata ppm
         */
        double dppm = tweak_ppm - nvdata.sntp.ppb * 1e-3;
        bool ppm_differs = fabs(dppm) > stats_y_95pct_confidence_interval(ppms);

        if (ppm_differs && stats_n(ppms) == nStats &&
            tv->tv_sec > nvdata.sntp.ppb_time + nvdata.sntp.interval_max_s * 10)
        {
            const char *desc = "differs from";

            if (saved_state.nv_ppm_changed_count < 2)
            {
                incr_nv_ppm_changed_count = true;
            }
            else
            {
                /* remember stable ppm time and value in case of reboot */
                desc = "replaces";
                nvdata.sntp.ppb_time = rtc_ppb_time;
                nvdata.sntp.ppb = rtc_ppb;
                flash_write(&nvdata);
                flash_writes++;
            }

            if (verbose)
            {
                ESP_LOGI(TAG, "   nv ppm %9.3f %s ppm", nvdata.sntp.ppb * 1e-3,
                    desc);
            }
        }
    }

    if (incr_nv_ppm_changed_count)
    {
        saved_state.nv_ppm_changed_count++;
    }
    else
    {
        saved_state.nv_ppm_changed_count = 0;
    }

    /* Send log data to mqtt server */

    if (health.mqtt)
    {
        struct mqtt_stats_s stats;

        stats.now = *tv_now;
        stats.elapsed = elapsed;
        stats.sntp_error = sntp_error;
        stats.tweak_sum = tweak.sum;
        stats.tweak_ppm = predicted_ppm;
        stats.error = error;
        stats.ppm = ppm;
        stats.sum_tweak_sum = saved_state.sum_tweak;
        stats.sum_sntp_error = saved_state.sum_sntp;
        stats.stat_ppm = stats_mean(ppms);
        stats.stat_stderr = stats_y_stderr(ppms);
        stats.stat_n = stats_n(ppms);
        stats.stat_n_valid = stats_n_valid(ppms);
        stats.converging_count = saved_state.converging_count;
        stats.flash_writes = flash_writes;

        mqtt_pub_statistics(&stats);
    }

    if (tv_now->tv_sec != tv->tv_sec || tv_now->tv_usec != tv->tv_usec)
    {
        if (!valid)
        {
            if (verbose)
            {
                ESP_LOGI(TAG, "%s: adjtime %.6fs skipped, point is outlier",
                    __func__, sntp_error);
            }
        }
        else if (adjtime(delta, NULL) == -1)
        {
            ESP_LOGW(TAG, "%s: adjtime %.6fs failed, what to do?", __func__,
                sntp_error);

            settimeofday(tv, NULL);
#if 0
            sntp_set_sync_status(SNTP_SYNC_STATUS_COMPLETED);
#endif
        }
        else
        {
            if (verbose)
            {
                ESP_LOGI(TAG, "%s: adjtime %.6fs started", __func__,
                    sntp_error);
            }
        }
    }

    /*
     * must always start tweak even if ppm is zero; this clears any
     * counters so they wont be added multiple times
     */

    tweak_start(tv, tweak_ppm);

    if (verbose)
    {
        ESP_LOGI(TAG,
            "   measured  ppm %9.3f from error %9.6f, interval %d, iter %d, tweak %s %9.3f",
            ppm, error, interval / 1000, saved_state.converging_count,
            (tweak_use_nv_ppm || tweak_use_stat_ppm) ? "yes" : "no", tweak.ppm);

        ESP_LOGI(TAG,
            "   statistic ppm %9.3f stderr %7.3f, n %2d, valid %2d, flash_w %3d",
            stats_mean(ppms), stats_y_stderr(ppms), stats_n(ppms),
            stats_n_valid(ppms), flash_writes);
    }

    sntp_count++;
}

/*
 * Custom time update method.
 * Derived from esp-idf/components/lwip/apps/sntp/sntp.c
 * parameter tv is the target time to set
 */
void sntp_sync_time(struct timeval *tv)
{
    struct timeval tv_now;
    struct timeval tv_delta;
    int64_t delta;

    gettimeofday(&tv_now, NULL);
    tv_subtract(tv, &tv_now, &tv_delta);

    delta = tv_to_us(&tv_delta);

    /*
     * There's large deltas which we use settimeofday for,
     * there's outliers which we use don't adjtime right away,
     * and there's small valid deltas which we adjtime at once.
     */

    bool large = llabs(delta) > 500000LL;

    if (large)
    {
        ESP_LOGW(TAG,
            "large delta. SNTP %ld.%06ld now %ld.%06ld delta %.6f",
            tv->tv_sec, tv->tv_usec, tv_now.tv_sec, tv_now.tv_usec,
            delta * 1e-6);

        settimeofday(tv, NULL);
        tv_now = *tv;

        sntp_set_sync_status(SNTP_SYNC_STATUS_COMPLETED);

        ESP_LOGI(TAG, "%s: settimeofday", __func__);

        if (large > 0)
        {
            counter_inc_t(C_TIME_LARGE_POS, tv->tv_sec);
        }
        else
        {
            counter_inc_t(C_TIME_LARGE_NEG, tv->tv_sec);
        }
    }
    else
    {
        sntp_set_sync_status(SNTP_SYNC_STATUS_IN_PROGRESS);

        if (verbose)
        {
            ESP_LOGI(TAG, "%s: adjtime %.6fs", __func__, delta * 1e-6);
        }
    }

    time_sync_notification_cb(tv, &tv_now, &tv_delta);
}

/******************************
 * Timing helpers
 ******************************/

/* sleep until just after the start of the next real time second. */
static void delay_to_next_second(void)
{
    struct timeval tv_now;

    gettimeofday(&tv_now, NULL);

    int ms_remaining = ((1000000L - tv_now.tv_usec) + 999) / 1000;
    int ticks = ms_to_ticks(ms_remaining);

#if 0
    ESP_LOGI(TAG, "Time %ld.%06ld; sleeping %d ms, %d ticks",
        tv_now.tv_sec, tv_now.tv_usec, ms_remaining, ticks);
#endif

    /* adding + 1 avoids waking up before the start of the second */
    vTaskDelay(ticks + 1);
}

/* Return true if time set. If not, tm_year will be (1970 - 1900). */
static bool is_time_set(void)
{
    time_t now;
    struct tm timeinfo;

    time(&now);
    localtime_r(&now, &timeinfo);

    if (timeinfo.tm_year >= (2016 - 1900))
    {
        health.time = 1;
        return true;
    }

    health.time = 0;
    return false;
}

/******************************
 * Time and network implementation
 ******************************/

/* Prepare system and networking needed to later start SNTP */
static void net_init(void)
{
    if (verbose)
    {
        ESP_LOGI(TAG, "%s: esp_netif_init", __func__);
    }

    ESP_ERROR_CHECK(esp_netif_init());

    if (verbose)
    {
        ESP_LOGI(TAG, "%s: esp_event_loop_create_default", __func__);
    }

    ESP_ERROR_CHECK(esp_event_loop_create_default());

    /* This helper function configures Wi-Fi or Ethernet, as selected in menuconfig.
     * Read "Establishing Wi-Fi or Ethernet Connection" section in
     * examples/protocols/README.md for more information about this function.
     */

    if (verbose)
    {
        ESP_LOGI(TAG, "%s: wifi_connect", __func__);
    }

    ESP_ERROR_CHECK(wifi_connect(nvdata.wifi.ssid, nvdata.wifi.password));
}

/* configure and start SNTP */
static void sntp_configure(void)
{
    static bool first_time = true;

    if (!first_time)
    {
        return;
    }

    first_time = false;

    if (verbose)
    {
        ESP_LOGI(TAG, "%s: sntp configure", __func__);
    }

    sntp_setoperatingmode(SNTP_OPMODE_POLL);
    sntp_setservername(0, nvdata.sntp.server);
    sntp_set_time_sync_notification_cb(NULL);
    sntp_set_sync_mode(SNTP_SYNC_MODE_SMOOTH);
}

static void sntp_health_check(void)
{
    struct timespec now;

    clock_gettime(CLOCK_REALTIME, &now);

    /* maintain the sync interval if we're not overdue */
    if (sntp_time_expected != 0 &&
        now.tv_sec < sntp_time_expected + nvdata.sntp.interval_min_s)
    {
        health.sntp = 1;
        return;
    }

    if (health.sntp)
    {
        counter_inc(C_SNTP_DC);
    }

    health.sntp = 0;            /* pre-init or overdue */
}

static void sntp_start(void)
{
    uint32_t interval = nvdata.sntp.interval_min_s * 1000;

    /* maintain the sync interval if we're not overdue */
    sntp_health_check();

    if (health.sntp)
    {
        interval = sntp_get_sync_interval();
    }

    sntp_count = 0;
    sntp_set_sync_interval(interval);

    if (verbose)
    {
        ESP_LOGI(TAG, "%s: sntp_init, interval %d", __func__, interval);
    }

    sntp_init();
}

void mqtt_connected_cb(void)
{
    health.mqtt = 1;
}

void mqtt_disconnected_cb(void)
{
    if (health.mqtt)
    {
        counter_inc(C_MQTT_DC);
    }

    health.mqtt = 0;
}

void wifi_connect_cb(void)
{
    ESP_LOGI(TAG, "%s: begin", __func__);

    /* enable SNTP operation and allow MQTT updates */

    health.wifi = 1;

    sntp_configure();
    sntp_start();

    mqtt_start(nvdata.sensors.mqtt_broker, nvdata.sensors.mqtt_site_room);

    if (strcmp(nvdata.sensors.mqtt_broker, "off") == 0)
    {
        health.mqtt = 1;
    }

    for (int n = 0; n < num_sensors; n++)
    {
        mqtt_pub_sensors_once(nvdata.sensors.period_s, n);
    }

    ESP_LOGI(TAG, "%s: end", __func__);
}

void wifi_disconnect_cb(void)
{
    /* disable SNTP operation and skip MQTT updates */
    ESP_LOGI(TAG, "%s: begin", __func__);

    if (health.wifi)
    {
        counter_inc(C_WIFI_DC);
    }

    health.wifi = 0;

    mqtt_stop();
    sntp_stop();

    ESP_LOGI(TAG, "%s: end", __func__);
}

int do_sensor(unsigned n)
{
    float f = 0.0;

    /* read temperature */
    if (htu21df_read_temp(i2c_h[n], &f) < 0)
    {
        ESP_LOGW(TAG,
            "%s: sensor %u read temp failed, value %f (prev. %f)",
            __func__, n, f, sensor_temp_c[n]);

        counter_inc(C_I2C_ERROR);

        return -1;
    }

    sensor_temp_c[n] = f;

    f = 0.0;

    /* read relative humidity */
    if (htu21df_read_rh(i2c_h[n], &f) < 0)
    {
        ESP_LOGW(TAG,
            "%s: sensor %u read rh failed, value %f (prev. %f)",
            __func__, n, f, sensor_rh[n]);

        counter_inc(C_I2C_ERROR);

        return -1;
    }

    /* Temperature Coefficient Compensation per HTU21D(F) Datasheet p. 4 */
    sensor_rh[n] = f - 0.15 * (25.0 - sensor_temp_c[n]);

    /* range check temperature */
    if (sensor_temp_c[n] < -40.0 || sensor_temp_c[n] > 80.00)
    {
        ESP_LOGW(TAG, " T%u: %5.2fC RH%u: %5.2f%%; T out of range", n,
            sensor_temp_c[n], n, sensor_rh[n]);

        counter_inc(C_TEMP_OOR);

        return -1;
    }

    /* range check RH */
    if (sensor_rh[n] < 10.0 || sensor_rh[n] > 101.0)
    {
        ESP_LOGW(TAG,
            " T%u: %5.2fC RH%u: %5.2f%%; RH out of range", n,
            sensor_temp_c[n], n, sensor_rh[n]);

        counter_inc(C_RH_OOR);

        return -1;
    }

    if (verbose)
    {
        ESP_LOGI(TAG, " T%u: %5.2fC RH%u: %5.2f%%", n, sensor_temp_c[n],
            n, sensor_rh[n]);
    }

    return 0;
}

/******************************
 * Main loop actions
 ******************************/

/* periodic update runs about once/second to update clock and maybe sample sensors */
void periodic_update(void)
{
    static bool first_time = true;
    struct timeval tv_now;
    struct tm timeinfo;
    bool adjusting = sntp_get_sync_status() == SNTP_SYNC_STATUS_IN_PROGRESS;
    i2c_handle bus = i2c_h[0];
    /* *INDENT-OFF* */
    const char *syncstr =
        !is_time_set() ? "setting time" :
        adjusting ? "adjusting time" : "synced";
    /* *INDENT-ON* */

    gettimeofday(&tv_now, NULL);
    localtime_r(&tv_now.tv_sec, &timeinfo);

    if (timeinfo.tm_sec == 0 || health.time == 0 || adjusting)
    {
        char t_buf[64];

        localtime_convert(t_buf, sizeof(t_buf), tv_now.tv_sec);

        if (adjusting)
        {
            ESP_LOGI(TAG, "Time: %s %s, %.6f", t_buf, syncstr, get_adjtime());
        }
        else if (!is_time_set())
        {
            ESP_LOGI(TAG, "Time: %s %s", t_buf, syncstr);
        }
    }

    if (!all_good && !adjusting && health.wifi && health.time && health.sntp
        && health.mqtt)
    {
        ESP_LOGI(TAG,
            "Time: WiFi, Time, SNTP and MQTT are healthy... turning off verbose");
        all_good = true;
        verbose = false;
    }

    /* state model */
    enum items_e
    { I_WIFI, I_SNTP, I_MQTT, I_TIME, I_TEMP_F, I_TEMP_C, I_RH };
    static uint8_t item = I_WIFI;
    static time_t elapsed = 0;
    static bool displayed_rh = false;

    /* top of every minute display time if set, otherwise wifi */
    if (first_time || timeinfo.tm_sec == 0)
    {
        displayed_rh = false;
        item = health.time ? I_TIME : I_WIFI;
        elapsed = 0;
    }

    /* determine next state */
    for (unsigned n = 0; n < 2; n++)
    {
        /* if WiFi healthy or shown long enough, move on */
        if (item == I_WIFI && (health.wifi || elapsed == 2))
        {
            item = I_SNTP;
            elapsed = 0;
        }

        /* if SNTP healthy or shown long enough, move on */
        if (item == I_SNTP && (health.sntp || elapsed == 2))
        {
            item = I_MQTT;
            elapsed = 0;
        }

        /* if MQTT healthy or shown long enough, move on */
        if (item == I_MQTT && (health.mqtt || elapsed == 2))
        {
            item = I_TIME;
            elapsed = 0;
        }

        /* if TIME not healthy or shown long enough, move on */
        if (item == I_TIME && (!health.time || elapsed == 6))
        {
            /* skip temp and rh if we've displayed it this minute already */
            item = displayed_rh ? I_WIFI : I_TEMP_F;
            elapsed = 0;
        }

        /* if temp in F shown long enough, move on */
        if (item == I_TEMP_F && elapsed == 2 && sensor_temp_c[0] != 0.0)
        {
            item = I_TEMP_C;
            elapsed = 0;
        }

        /* if temp in C shown long enough, move on */
        if (item == I_TEMP_C && elapsed == 2 && sensor_temp_c[0] != 0.0)
        {
            item = I_RH;
            elapsed = 0;
        }

        /* if RH shown long enough, move on */
        if (item == I_RH && elapsed == 2 && sensor_rh[0] != 0.0)
        {
            displayed_rh = true;
            item = I_WIFI;
            elapsed = 0;
        }
    }

    bool ampm = (nvdata.display.flags & DISPLAY_AMPM) == DISPLAY_AMPM;
    uint8_t dots = 0;
    dots |= (timeinfo.tm_sec >= 58 || (item == I_TIME && ampm &&
            timeinfo.tm_hour < 12)) ? TOP_LEFT_DOT : 0;
    dots |= (!all_good || adjusting || (item == I_TIME && ampm &&
            timeinfo.tm_hour >= 12)) ? BOTTOM_LEFT_DOT : 0;
    dots |= (item == I_TIME && timeinfo.tm_sec % 2 == 0) ? CENTER_COLON : 0;
    dots |= (item == I_TEMP_C || item == I_TEMP_F) ? TOP_RIGHT_DOT : 0;

    ht16k33_display_dots(bus, dots);

    /* display indication */
    if (elapsed == 0)
    {
        char buf[5];
        time_t min;             /* minutes since issue experienced */
        static uint8_t m;       /* minutes maxed with 99 */

        switch (item)
        {
            case I_WIFI:
                min = (tv_now.tv_sec - counter_t[C_WIFI_DC]) / 60;
                m = min <= 99 ? min : 99;

                if (min)
                {
                    snprintf(buf, sizeof(buf), "Co%02hhd", m);
                }
                else
                {
                    snprintf(buf, sizeof(buf), "Conn");
                }
                break;

            case I_SNTP:
                min = (tv_now.tv_sec - counter_t[C_SNTP_DC]) / 60;
                m = min <= 99 ? min : 99;

                if (min)
                {
                    snprintf(buf, sizeof(buf), "Sn%02hhd", m);
                }
                else
                {
                    snprintf(buf, sizeof(buf), "SntP");
                }
                break;

            case I_MQTT:
                min = (tv_now.tv_sec - counter_t[C_MQTT_DC]) / 60;
                m = min <= 99 ? min : 99;

                if (min)
                {
                    snprintf(buf, sizeof(buf), "Pu%02hhd", m);
                }
                else
                {
                    snprintf(buf, sizeof(buf), "PuSH");
                }
                break;

            case I_TIME:
            {
                uint8_t hour = timeinfo.tm_hour;

                if (ampm)
                {
                    hour = hour % 12;
                    hour = (hour == 0) ? 12 : hour;

                    snprintf(buf, sizeof(buf), "%2hhd%02hhd", hour,
                        timeinfo.tm_min);
                }
                else
                {
                    snprintf(buf, sizeof(buf), "%02hhd%02hhd", hour,
                        timeinfo.tm_min);
                }
            }

                break;

            case I_TEMP_F:
                snprintf(buf, sizeof(buf), "%3dF",
                    (int)rintf(sensor_temp_c[0] * 9 / 5 + 32));
                break;

            case I_TEMP_C:
                snprintf(buf, sizeof(buf), " %2dC",
                    (int)rintf(sensor_temp_c[0]));
                break;

            case I_RH:
                snprintf(buf, sizeof(buf), " %2dh", (int)rintf(sensor_rh[0]));
                break;
        }

        ht16k33_display_string(bus, buf);
    }

    elapsed++;

    bool sensors_now = first_time ||
        (timeinfo.tm_min * 60 + timeinfo.tm_sec) % nvdata.sensors.period_s == 0;

    if (sensors_now)
    {
        /* HTU21D(F) sensors */
        for (unsigned n = 0; n < num_sensors; n++)
        {
            if (do_sensor(n) == 0 && health.mqtt)
            {
                mqtt_pub_sensors(&tv_now.tv_sec, &timeinfo, sensor_rh[n],
                    sensor_temp_c[n], n);
            }
        }
    }

    if (!adjusting)
    {
        tweak_clock_periodic(&tv_now);
    }

    if (first_time)
    {
        first_time = false;
    }
}

static void add_points(double ppm, double t0, double dt, int n)
{
    if (t0 == 0.0)
    {
        return;
    }

    for (double t = t0 - (n - 1) * dt; t <= t0; t += dt)
    {
        stats_add_data(ppms, t, ppm, dt);
    }
}

static void display_task(void *arg)
{
    for (;;)
    {
        periodic_update();

#if 0                           /* test deep sleep */
        {
            struct timespec ts;
            clock_gettime(CLOCK_MONOTONIC, &ts);

            if (ts.tv_sec % 10 == 0)
            {
                ESP_LOGI(TAG, "clock sec %ld", ts.tv_sec);
            }

            if (stats_n_valid(ppms) >= 0.8 * nStats)
            {
                break;
            }
        }
#endif
#if 0                           /* test disconnect reconnect */
        {
            struct timespec ts;
            clock_gettime(CLOCK_MONOTONIC, &ts);

            if (ts.tv_sec % 10 == 0)
            {
                ESP_LOGI(TAG, "clock sec %ld", ts.tv_sec);
            }

            if (ts.tv_sec >= 300 && ts.tv_sec % 300 == 0)
            {
                ESP_ERROR_CHECK(wifi_disconnect());
            }

            if (ts.tv_sec > 300 && ts.tv_sec % 300 == 4)
            {
                ESP_ERROR_CHECK(wifi_connect(nvdata.wifi.ssid,
                        nvdata.wifi.password));
            }
        }
#endif

        delay_to_next_second();
    }
}

void display_task_create(void)
{
    const unsigned int STACK_SIZE = 4 * 1024;
    TaskHandle_t display_task_h;

    xTaskCreate(display_task, "display", STACK_SIZE, NULL, tskIDLE_PRIORITY + 3,
        &display_task_h);
}

/******************************
 * Console command processor
 ******************************/

static int cmd_flash(int argc, char **argv)
{
    flash_show(&nvdata);
    return 0;
}

static int cmd_reset(int argc, char **argv)
{
    int ticks = ms_to_ticks(500);

    printf("Reset in progress...\n");
    vTaskDelay(ticks + 1);
    esp_restart();
    return 0;                   /* not reached */
}

static int cmd_save(int argc, char **argv)
{
    flash_write(&nvdata);
    flash_writes++;
    return 0;
}

static bool valid_wifi_hostname(const char *s)
{
    int len = strlen(s);

    if (len == 0 || len >= FLASH_WIFI_HOSTNAME_LEN)
    {
        printf("error: hostname must be 1-%u characters\n",
            FLASH_WIFI_HOSTNAME_LEN - 1);
        return false;
    }

    if (*s == '-')
    {
        printf("error: hostname may not start with hyphen (-)\n");
        return false;
    }

    for (; *s; s++)
    {
        if (!isdigit(*s) && !isalpha(*s) && *s != '-')
        {
            printf("error: hostname may only contain a-z, 0-9, and -\n");
            return false;
        }
    }

    return true;
}

static bool valid_ip_address(const char *s)
{
    unsigned int a[4] = { 256, 256, 256, 256 };
    int n = sscanf(s, "%u.%u.%u.%u", a, a + 1, a + 2, a + 3);

    if (n != 4)
    {
        printf("error: ip address must be of form nnn.nnn.nnn.nnn\n");
        return false;
    }

    if (a[0] > 255 || a[1] > 255 || a[2] > 255 || a[3] > 255)
    {
        printf("error: ip address digits must be in range 0-255\n");
        return false;
    }

    if (a[0] == 0 && a[1] == 0 && a[2] == 0 && a[3] == 0)
    {
        printf("error: ip address 0.0.0.0 is not a valid host address\n");
        return false;
    }

    if (a[0] == 255 && a[1] == 255 && a[2] == 255 && a[3] == 255)
    {
        printf
            ("error: ip address 255.255.255.255 is not a valid host address\n");
        return false;
    }

    if (a[0] == 127)
    {
        printf
            ("error: ip address 127.nnn.nnn.nnn is not a valid host address\n");
        return false;
    }

    return true;
}

static bool valid_wifi_ssid(const char *s)
{
    if (strlen(s) == 0 || strlen(s) >= FLASH_WIFI_SSID_LEN)
    {
        printf("error: wifi ssid must be 1-%u characters\n",
            FLASH_WIFI_SSID_LEN - 1);
        return false;
    }

    return true;
}

static bool valid_wifi_password(const char *s)
{
    if (strlen(s) < 8 || strlen(s) >= FLASH_WIFI_PASSWORD_LEN)
    {
        printf("error: wifi password must be 8-%u characters\n",
            FLASH_WIFI_PASSWORD_LEN - 1);
        return false;
    }

    return true;
}

static bool valid_sensors_siterm(const char *s)
{
    int len = strlen(s);

    if (len < 3 || len >= FLASH_SENSOR_SITERM_LEN)
    {
        printf("error: sensors site/room must be 3-%u characters\n",
            FLASH_SENSOR_SITERM_LEN - 1);
        return false;
    }

    if (s[0] == '/' || s[len - 1] == '/' || strchr(s, '/') != strrchr(s, '/'))
    {
        printf("error: sensors site/room must be in form site/room \n");
        return false;
    }

    return true;
}

static bool valid_sensors_period(const char *s)
{
    unsigned int period = 0;
    int n = sscanf(s, "%u", &period);

    if (n != 1 || period == 0 || period % 60 != 0)
    {
        printf
            ("error: sensor period must be a positive multiple of 60 seconds\n");
        return false;
    }

    return true;
}

static bool valid_display_tz(const char *s)
{
    int len = strlen(s);

    if (len < 3 || len >= FLASH_TZ_LEN)
    {
        printf("error: timezone must be 3-%u characters\n", FLASH_TZ_LEN - 1);
        return false;
    }

    if (len == 3 && strcmp(s, "UTC") != 0)
    {
        printf("error: timezone of 3 characters must be UTC\n");
        return false;
    }

    for (; isalpha(*s); s++)
    {
        ;
    }

    if (*s == '+' || *s == '-')
    {
        s++;
    }

    if (!isdigit(*s))
    {
        printf
            ("error: timezone initial alphabetic characters must be followed by a numeric digit\n");
    }

    return true;
}

static bool valid_verbose(const char *s)
{
    if ((*s != '1' && *s != '0') || *++s != '\0')
    {
        return false;
    }

    return true;
}

static int cmd_set(int argc, char **argv)
{
    if (argc != 3)
    {
        printf("usage: set <item_name> <new_value>\n");
        return -1;
    }

    const char *item_name = argv[1];
    const char *value = argv[2];

    if (strcmp(item_name, FLASH_WIFI_HOSTNAME) == 0)
    {
        if (!valid_wifi_hostname(value))
        {
            printf("error: %s must be a valid hostname (ex. c_office3)\n",
                item_name);
            return -1;
        }
        snprintf(nvdata.wifi.hostname, sizeof(nvdata.wifi.hostname), "%s",
            value);
        return 0;
    }

    if (strcmp(item_name, FLASH_WIFI_SSID) == 0)
    {
        if (!valid_wifi_ssid(value))
        {
            printf("error: %s must be a valid SSID (ex. weavings)\n",
                item_name);
            return -1;
        }
        snprintf(nvdata.wifi.ssid, sizeof(nvdata.wifi.ssid), "%s", value);
        return 0;
    }

    if (strcmp(item_name, FLASH_WIFI_PASSWORD) == 0)
    {
        if (!valid_wifi_password(value))
        {
            printf
                ("error: %s must be a valid WiFi Password (ex. joe14bob893)\n",
                item_name);
            return -1;
        }
        snprintf(nvdata.wifi.password, sizeof(nvdata.wifi.password), "%s",
            value);
        return 0;
    }

    if (strcmp(item_name, FLASH_SENSORS_BROKER) == 0)
    {
        if (strcmp(value, "off") != 0 && !valid_ip_address(value))
        {
            printf
                ("error: %s must be a valid IP address (ex. 192.168.254.31) or 'off'\n",
                item_name);
            return -1;
        }
        snprintf(nvdata.sensors.mqtt_broker, sizeof(nvdata.sensors.mqtt_broker),
            "%s", value);
        return 0;
    }

    if (strcmp(item_name, FLASH_SENSORS_SITERM) == 0)
    {
        if (!valid_sensors_siterm(value))
        {
            printf
                ("error: %s must be a valid site/room (ex. whitehouse/blue_room)\n",
                item_name);
            return -1;
        }
        snprintf(nvdata.sensors.mqtt_site_room,
            sizeof(nvdata.sensors.mqtt_site_room), "%s", value);
        return 0;
    }

    if (strcmp(item_name, FLASH_SENSORS_PERIOD) == 0)
    {
        if (!valid_sensors_period(value))
        {
            printf
                ("error: %s must be a valid sensors period in seconds (ex. 300)\n",
                item_name);
            return -1;
        }
        nvdata.sensors.period_s = atoi(value);
        return 0;
    }

    if (strcmp(item_name, FLASH_SNTP_SERVER) == 0)
    {
        if (!valid_ip_address(value))
        {
            printf
                ("error: %s must be a valid IP address (ex. 192.168.254.31)\n",
                item_name);
            return -1;
        }
        snprintf(nvdata.sntp.server, sizeof(nvdata.sntp.server), "%s", value);
        return 0;
    }

    if (strcmp(item_name, FLASH_DISPLAY_TZ) == 0)
    {
        if (!valid_display_tz(value))
        {
            printf
                ("error: %s must be a valid timezone (ex. PST8PDT,M3.2.0/2,M11.1.0)\n",
                item_name);
            return -1;
        }
        snprintf(nvdata.display.tz, sizeof(nvdata.display.tz), "%s", value);
        setenv("TZ", nvdata.display.tz, 1);
        tzset();
        return 0;
    }

    if (strcmp(item_name, "verbose") == 0)
    {
        if (!valid_verbose(value))
        {
            printf("error: %s must be 1 (on) or 0 (off)\n", item_name);
            return -1;
        }
        verbose = *value == '1' ? true : false;
        return 0;
    }

    if (strcmp(item_name, FLASH_SNTP_INTERVAL_MIN) == 0 ||
        strcmp(item_name, FLASH_SNTP_INTERVAL_MAX) == 0 ||
        strcmp(item_name, FLASH_SNTP_ERRPPM_MAX) == 0 ||
        strcmp(item_name, FLASH_SNTP_STDERR_MAX) == 0 ||
        strcmp(item_name, FLASH_SNTP_PPB) == 0 ||
        strcmp(item_name, FLASH_SNTP_PPB_TIME) == 0 ||
        strcmp(item_name, FLASH_DISPLAY_FLAGS) == 0 ||
        strcmp(item_name, FLASH_DISPLAY_BRIGHT) == 0 ||
        strcmp(item_name, FLASH_DISPLAY_TZ_OFF) == 0)
    {
        printf("error: %s may not be set by the user\n", item_name);
        return -1;
    }

    printf
        ("error: unknown item name, see first column of 'show' command output\n");
    return -1;
}

static int cmd_status(int argc, char **argv)
{
    sntp_health_check();

    printf("%-15s  %u\n", "verbose", verbose ? 1 : 0);
    printf("%-15s  WiFi %u, Time %u, SNTP %u, MQTT %u\n", "Subsystems",
        health.wifi ? 1 : 0, health.time ? 1 : 0, health.sntp ? 1 : 0,
        health.mqtt ? 1 : 0);

    {
        printf("%-15s ", "Buttons");

        for (int n = 0; n < NUM_BUTTONS; n++)
        {
            int val = button_get_level(&button[n]);
            printf(" %d", val);
        }

        printf("\n");
    }

    printf("%-15s  %5s %10s %s\n", "Counters", "Count", "Time", "Local time");

    for (unsigned n = 0; n < C_N_ENTRIES; n++)
    {
        char t_buf[64];

        *t_buf = '\0';

        if (counter_t[n] != 0)
        {
            localtime_convert(t_buf, sizeof(t_buf), counter_t[n]);
        }

        printf(" %-14s  %5hu %10lu %s\n", counter_desc[n], counter_n[n],
            counter_t[n], t_buf);
    }

    {
        struct timespec runtime;

        clock_gettime(CLOCK_MONOTONIC, &runtime);

        unsigned long min = runtime.tv_sec / 60;
        unsigned long hr = min / 60;
        unsigned long day = hr / 24;
        uint8_t _sec = runtime.tv_sec % 60;
        uint8_t _min = min % 60;
        uint8_t _hr = hr % 24;

        printf("%-15s  %lu.%03lu : %lud %02u:%02u:%02u\n", "Runtime",
            runtime.tv_sec, runtime.tv_nsec / 1000000, day, _hr, _min, _sec);
    }

    {
        struct timespec now;

        clock_gettime(CLOCK_REALTIME, &now);

        printf("%-15s  %lu.%03lu\n", "Systime", now.tv_sec,
            now.tv_nsec / 1000000);
    }

    {
        struct timeval tv_now;
        char t_buf[64];

        gettimeofday(&tv_now, NULL);
        localtime_convert(t_buf, sizeof(t_buf), tv_now.tv_sec);
        printf("%-15s  %s\n", "Localtime", t_buf);
    }

    for (uint8_t n = 0; n < num_sensors; n++)
    {
        char buf[11];
        snprintf(buf, sizeof(buf), "Sensor %hhu", n);
        printf("%-15s  Temp %.2fC %.2fF, RH %.2f%%\n", buf, sensor_temp_c[n],
            sensor_temp_c[n] * 9 / 5 + 32, sensor_rh[n]);
    }

    return 0;
}

static int task_sort(const TaskStatus_t * p, const TaskStatus_t * q)
{
    return p->eCurrentState < q->eCurrentState ? 1 :
        p->eCurrentState > q->eCurrentState ? -1 :
        p->ulRunTimeCounter > q->ulRunTimeCounter ? 1 :
        p->ulRunTimeCounter < q->ulRunTimeCounter ? -1 :
        strcmp(p->pcTaskName, q->pcTaskName);
}

/* bubble sort - slower but less memory than qsort(3) */
static void tasks_sort(TaskStatus_t * tasks, int n)
{
    int i;
    int j;

    for (i = 0; i < n - 1; i++)
    {
        for (j = i + 1; j < n; j++)
        {
            if (task_sort(tasks + i, tasks + j) < 0)
            {
                TaskStatus_t t = tasks[i];
                tasks[i] = tasks[j];
                tasks[j] = t;
            }
        }
    }
}

static int cmd_tasks(int argc, char **argv)
{
    unsigned int nTasks = uxTaskGetNumberOfTasks();
    TaskStatus_t *tasks, *p;
    struct timespec trun;       /* posix runtime */
    float runtime;
    int n;
    const float MAX_S = (unsigned long)(-1) * 1e-6;

    clock_gettime(CLOCK_MONOTONIC, &trun);
    runtime = trun.tv_sec + trun.tv_nsec * 1e-9;

    {
        uint32_t us;            /* overflows at 71.5 min */

        tasks = (TaskStatus_t *) pvPortMalloc(nTasks * sizeof(TaskStatus_t));
        n = uxTaskGetSystemState(tasks, nTasks, &us);
        assert(n == nTasks);
    }

    printf("Task Status: %u tasks, %.03fs total run time\n", n, runtime);
    printf("%-15s %2s %-9s %2s %2s %13s %6s%% %s\n", "Task name", "ID",
        "State", "CP", "BP", "Runtime", "Run", "Stack Unused");

    tasks_sort(tasks, n);

    for (p = tasks; n > 0; n--, p++)
    {
        float task_s = p->ulRunTimeCounter * 1e-6;
        bool idle_overrun = false;

        if (strcmp(p->pcTaskName, "IDLE") == 0 && runtime >= MAX_S)
        {
            idle_overrun = true;
        }

        float runPct = idle_overrun ? 0.0 : task_s * 1e2 / runtime;

        const char *taskState =
            p->eCurrentState == eReady ? "Ready" : p->eCurrentState ==
            eRunning ? "Running" : p->eCurrentState ==
            eBlocked ? "Blocked" : p->eCurrentState ==
            eSuspended ? "Suspended" : p->eCurrentState ==
            eDeleted ? "Deleted" : "Unknown";

        if (idle_overrun)
        {
            printf("%-15s %2u %-9s %2u %2u %13s %6s  %u\n",
                p->pcTaskName, p->xTaskNumber, taskState, p->uxCurrentPriority,
                p->uxBasePriority, "-.---", "-.--", p->usStackHighWaterMark);
        }
        else
        {
            printf("%-15s %2u %-9s %2u %2u %13.03f %6.2f%% %u\n",
                p->pcTaskName, p->xTaskNumber, taskState, p->uxCurrentPriority,
                p->uxBasePriority, task_s, runPct, p->usStackHighWaterMark);
        }
    }

    vPortFree(tasks);

    return 0;
}

static void register_flash_cmds(void)
{
    const esp_console_cmd_t flash_cmd = {
        .command = "flash",
        .help = "Show contents of configuration flash.",
        .hint = NULL,
        .func = &cmd_flash,
        .argtable = NULL,
    };

    ESP_ERROR_CHECK(esp_console_cmd_register(&flash_cmd));

    const esp_console_cmd_t reset_cmd = {
        .command = "reset",
        .help = "Reset the CPU.",
        .hint = NULL,
        .func = &cmd_reset,
        .argtable = NULL,
    };

    ESP_ERROR_CHECK(esp_console_cmd_register(&reset_cmd));

    const esp_console_cmd_t save_cmd = {
        .command = "save",
        .help = "Commit changes from 'set' to configuration flash.",
        .hint = NULL,
        .func = &cmd_save,
        .argtable = NULL,
    };

    ESP_ERROR_CHECK(esp_console_cmd_register(&save_cmd));

    const esp_console_cmd_t set_cmd = {
        .command = "set",
        .help = "Set a configuration flash entry.",
        .hint = NULL,
        .func = &cmd_set,
        .argtable = NULL,
    };

    ESP_ERROR_CHECK(esp_console_cmd_register(&set_cmd));

    const esp_console_cmd_t status_cmd = {
        .command = "status",
        .help = "Show current subsystem status.",
        .hint = NULL,
        .func = &cmd_status,
        .argtable = NULL,
    };

    ESP_ERROR_CHECK(esp_console_cmd_register(&status_cmd));

    const esp_console_cmd_t tasks_cmd = {
        .command = "tasks",
        .help = "Show task status.",
        .hint = NULL,
        .func = &cmd_tasks,
        .argtable = NULL,
    };

    ESP_ERROR_CHECK(esp_console_cmd_register(&tasks_cmd));
}

void repl_start(void)
{
    esp_console_repl_t *repl = NULL;
    esp_console_repl_config_t repl_config = ESP_CONSOLE_REPL_CONFIG_DEFAULT();

    repl_config.prompt = "sntp-clock>";

    // install console REPL environment
    esp_console_dev_uart_config_t uart_config =
        ESP_CONSOLE_DEV_UART_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_console_new_repl_uart(&uart_config, &repl_config,
            &repl));

    register_flash_cmds();

    printf("\n");
    printf(" ==============================================================\n");
    printf(" | SNTP Clock - type help for help                            |\n");
    printf(" ==============================================================\n");
    printf("\n");

    // start console REPL
    ESP_ERROR_CHECK(esp_console_start_repl(repl));

}

/******************************
 * Main application
 ******************************/

/* Main function for application */
void app_main(void)
{
    ++boot_count;
    ESP_LOGI(TAG, "Boot count: %d", boot_count);

    flash_init();
    flash_read(&nvdata);

#if 1                           /* hack */
    if (nvdata.sntp.interval_max_s != SNTP_INTERVAL_MAX_S_DEF)
    {
        nvdata.sntp.interval_max_s = SNTP_INTERVAL_MAX_S_DEF;
    }
#endif

    /* initialize i2c bus controllers */
    for (int n = 0; n < NUM_I2C_BUSES; n++)
    {
        i2c_h[n] = i2c_start(&i2c_config[n]);
    }

    /* initialize i2c bus devices */
    for (int n = 0; n < NUM_I2C_BUSES; n++)
    {
        if (htu21df_reset(i2c_h[n]) < 0)
        {
            counter_inc(C_I2C_ERROR);
            if (n == NUM_SENSORS - 1)
            {
                num_sensors--;  /* ignore last sensor if failed */
            }
        }
    }

    if (boot_count == 1)
    {
        ht16k33_init(i2c_h[0], nvdata.display.flags, nvdata.display.brightness);
    }

    if (!is_time_set())
    {
        ESP_LOGI(TAG, "Time uninitialized. WiFi, SNTP, set time.");
        ht16k33_display_string(i2c_h[0], "init");
    }
    else
    {
        ESP_LOGI(TAG, "Time initialized. WiFi, SNTP, adjust time.");
        ht16k33_display_string(i2c_h[0], "adj");
    }

    /* needed before sntp_configure() as the sntp event callback uses it */
    /* TODO: alternate timezones, changing timezones during runtime */
    setenv("TZ", nvdata.display.tz, 1);
    tzset();

    /*
     * init statistics, seed prior good ppm from RTC and NVS if
     * available.
     */
    ppms = stats_start(nStats);

    add_points(rtc_ppb * 1e-3, rtc_ppb_time, rtc_ppb_elapsed, nStats * 0.167);
    add_points(nvdata.sntp.ppb * 1e-3, nvdata.sntp.ppb_time,
        nvdata.sntp.interval_max_s, nStats * 0.333);

    for (int n = 0; n < NUM_BUTTONS; n++)
    {
        button_start(&button[n]);
    }

    /* TODO: consider async thread for network ops */
    net_init();
    tweak_init();

    display_task_create();
    repl_start();

    /* wait forever (until it's time to exit) */
    vTaskSuspend(NULL);

    /* TODO: currently no way to get here */

    /* orderly shutdown */
    if (health.wifi)
    {
        ESP_ERROR_CHECK(wifi_disconnect());
    }

    for (int n = 0; n < NUM_I2C_BUSES; n++)
    {
        i2c_stop(i2c_h[n]);
    }

    stats_stop(ppms);

    const int deep_sleep_ms = 10;
    //ESP_LOGI(TAG, "Entering deep sleep for %d ms", deep_sleep_ms);

    esp_deep_sleep(1000LL * deep_sleep_ms);
}
