/* SNTP LED clock application, statistics API

*/

#define nStats 45

struct _stats_s;

typedef struct _stats_s *stats_t;

/* *INDENT-OFF* */

/*
 * Initialize and return a statistics object capable of handling max
 * 2-D data points.
 */
stats_t stats_start(uint8_t max) __attribute((warn_unused_result));

/* *INDENT-ON* */

/*
 * Deallocate statistics object st.
 */
void stats_stop(stats_t st) __attribute__((nonnull));

/*
 * Add data point to statistics object st. If this would store more
 * than max (passed in at stats_start), the oldest data is
 * overwritten instead of growing the data further. Performs a least
 * squares linear regression on the stored points. w is the weight of
 * the data. Returns true if data is in the linear regression valid set
 * or false if data is an outlier.
 */
bool stats_add_data(stats_t st, double x, double y, double w)
    __attribute__((nonnull));

/*
 * Return the number of data points added (limited to max passed in
 * at stats_start).
 */
uint8_t stats_n(const stats_t st) __attribute__((nonnull));

/*
 * Return the number of data points which haven't been thrown out.
 */
uint8_t stats_n_valid(const stats_t st) __attribute__((nonnull));

/*
 * Return standard deviation of y data.
 */
double stats_y_stddev(const stats_t st) __attribute__((nonnull));

/*
 * Return standard error of y data.
 */
double stats_y_stderr(const stats_t st) __attribute__((nonnull));

/*
 * Return 95% confidence interval of y data.
 */
double stats_y_95pct_confidence_interval(const stats_t st)
    __attribute__((nonnull));

/*
 * Return slope of linear regression line calculated by stats_add_data.
 */
double stats_slope(const stats_t st) __attribute__((nonnull));

/*
 * Return y intercept of linear regression line calculated by
 * stats_add_data.
 */
double stats_intercept(const stats_t st) __attribute__((nonnull));

/*
 * Return the predicted y value at position x of linear regression line
 * calculated by stats_add_data.
 */
double stats_y_at_x(const stats_t st, double x) __attribute__((nonnull));

/*
 * Return the predicted y value at an x position midway between the
 * minimum and maximum x values stored in st, using the linear regression
 * line calculated by stats_add_data. Since this position is at the
 * center of the data set, it represents the center of mass of the data
 * points and changes very slowly.
 */
double stats_mean(const stats_t st) __attribute__((nonnull));
