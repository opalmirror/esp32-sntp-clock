/* SNTP LED clock application, button API

*/

typedef void (*callback_fcn_t)(const void *);

typedef struct
{
    uint8_t n;
    uint8_t gpio;
    callback_fcn_t callback_fcn;
} button_t;

int button_get_level(const button_t * p) __attribute((nonnull(1)));

void button_start(const button_t * p) __attribute((nonnull(1)));

void button_stop(const button_t * p) __attribute((nonnull(1)));
