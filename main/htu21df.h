/* SNTP LED clock application, HTU21D(F) chip API

*/

int htu21df_reset(const i2c_handle h) __attribute__((nonnull(1)));

int htu21df_read_rh(const i2c_handle h, float *prh)
    __attribute__((nonnull(1, 2)));

int htu21df_read_temp(const i2c_handle h, float *ptemp_c)
    __attribute__((nonnull(1, 2)));
