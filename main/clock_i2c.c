/* I2C for SNTP LED clock application, common i2c functions

*/

#include "freertos/FreeRTOS.h"
#include "driver/i2c.h"
#include "esp_event.h"
#include "esp_log.h"
#include "clock_i2c.h"

#define DEBUG_I2C 0

/**************************************
 * private definitions
 **************************************/

#define I2C_MASTER_TX_BUF_DISABLE 0 /*!< I2C master doesn't need buffer */
#define I2C_MASTER_RX_BUF_DISABLE 0 /*!< I2C master doesn't need buffer */
#define WRITE_BIT I2C_MASTER_WRITE  /*!< I2C master write */
#define READ_BIT I2C_MASTER_READ    /*!< I2C master read */
#define ACK_CHECK_EN 0x1        /*!< I2C master will check ack from slave */
#define ACK_CHECK_DIS 0x0       /*!< I2C master will not check ack from slave */
#define ACK_VAL 0x0             /*!< I2C ack value */
#define NACK_VAL 0x1            /*!< I2C nack value */

/**************************************
 * private data
 **************************************/

static const char *TAG = "clock_i2c";

/**************************************
 * private functions
 **************************************/

/**************************************
 * public functions
 **************************************/

i2c_handle i2c_start(const clock_i2c_t * p)
{
    i2c_driver_install(p->port, I2C_MODE_MASTER, I2C_MASTER_RX_BUF_DISABLE,
        I2C_MASTER_TX_BUF_DISABLE, 0);

    i2c_config_t conf = {
        .mode = I2C_MODE_MASTER,
        .sda_io_num = p->gpio_sda,
        .sda_pullup_en = GPIO_PULLUP_ENABLE,
        .scl_io_num = p->gpio_scl,
        .scl_pullup_en = GPIO_PULLUP_ENABLE,
        .master.clk_speed = p->frequency,
        // .clk_flags = 0,          /*!< Optional, you can use I2C_SCLK_SRC_FLAG_* flags to choose i2c source clock here. */
    };

    (void)i2c_param_config(p->port, &conf);

    return p;
}

void i2c_stop(const i2c_handle h)
{
    i2c_driver_delete(h->port);
}

int i2c_addr_write(const i2c_handle h, uint8_t chip_addr, uint8_t reg,
    const uint8_t * data, int len)
{
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);

    i2c_master_write_byte(cmd, chip_addr << 1 | WRITE_BIT, ACK_CHECK_EN);
    i2c_master_write_byte(cmd, reg, ACK_CHECK_EN);

    for (int i = 0; i < len; i++)
    {
        i2c_master_write_byte(cmd, data[i], ACK_CHECK_EN);
    }

    i2c_master_stop(cmd);

    esp_err_t ret = i2c_master_cmd_begin(h->port, cmd, 1000 / portTICK_RATE_MS);

    i2c_cmd_link_delete(cmd);

    if (ret == ESP_OK)
    {
#if DEBUG_I2C
        ESP_LOGI(TAG, "%s: 0x%02x W 0x%02x %3d count", __func__, chip_addr, reg,
            len);
#endif
        return 0;
    }

    const char *s = (ret == ESP_ERR_TIMEOUT) ? "Bus timeout" : "Write failed";
    ESP_LOGW(TAG, "%s: 0x%02x R 0x%02x %3d count: %s", __func__, chip_addr, reg,
        len, s);

    return -1;
}

int i2c_addr_read(const i2c_handle h, uint8_t chip_addr, uint8_t * data,
    int len)
{
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);

    i2c_master_write_byte(cmd, chip_addr << 1 | READ_BIT, ACK_CHECK_EN);

    if (len > 1)
    {
        i2c_master_read(cmd, data, len - 1, ACK_VAL);
    }

    i2c_master_read_byte(cmd, data + len - 1, NACK_VAL);
    i2c_master_stop(cmd);

    esp_err_t ret = i2c_master_cmd_begin(h->port, cmd, 1000 / portTICK_RATE_MS);

    i2c_cmd_link_delete(cmd);

    if (ret == ESP_OK)
    {
#if DEBUG_I2C
        for (int i = 0; i < len; i++)
        {
            ESP_LOGI(TAG, "%s: 0x%02x R 0x%02x: 0x%02x", __func__, chip_addr, i,
                data[i]);
        }
#endif
        return 0;
    }

    const char *s = (ret == ESP_ERR_TIMEOUT) ? "Bus timeout" : "Read failed";
    ESP_LOGW(TAG, "%s: 0x%02x R %3d count: %s", __func__, chip_addr, len, s);

    return -1;
}

int i2c_addr_write_read(const i2c_handle h, uint8_t chip_addr, uint8_t reg,
    uint8_t * data, int read_len)
{
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, chip_addr << 1 | WRITE_BIT, ACK_CHECK_EN);

    i2c_master_write_byte(cmd, reg, ACK_CHECK_EN);
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, chip_addr << 1 | READ_BIT, ACK_CHECK_EN);

    if (read_len > 1)
    {
        i2c_master_read(cmd, data, read_len - 1, ACK_VAL);
    }

    i2c_master_read_byte(cmd, data + read_len - 1, NACK_VAL);
    i2c_master_stop(cmd);

    esp_err_t ret = i2c_master_cmd_begin(h->port, cmd, 1000 / portTICK_RATE_MS);

    i2c_cmd_link_delete(cmd);

    if (ret == ESP_OK)
    {
#if DEBUG_I2C
        ESP_LOGI(TAG, "%s: 0x%02x W 0x%02x   0 count", __func__, chip_addr,
            reg);

        for (int i = 0; i < read_len; i++)
        {
            ESP_LOGI(TAG, "%s: 0x%02x R 0x%02x: 0x%02x", __func__, chip_addr, i,
                data[i]);
        }
#endif
        return 0;
    }

    const char *s =
        (ret == ESP_ERR_TIMEOUT) ? "Bus timeout" : "Write read failed";
    ESP_LOGW(TAG, "%s: 0x%02x R 0x%02x %3d count: %s", __func__, chip_addr, reg,
        read_len, s);

    return -1;
}
