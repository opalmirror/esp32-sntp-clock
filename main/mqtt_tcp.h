/* SNTP LED clock application, common MQTT over TCP API

*/

struct mqtt_stats_s
{
    struct timeval now;         /* gettimeofday */

    /* parameters collected since last SNTP poll */

    double elapsed;             /* time elapsed since last poll, seconds */
    double sntp_error;          /* error (just SNTP adjtime), seconds */
    double tweak_sum;           /* error (just pre-corrected), seconds */
    double tweak_ppm;           /* error (just pre-corrected), parts per million */
    double error;               /* error (SNTP adjtime plus pre-corrected), seconds */
    double ppm;                 /* error (SNTP adjtime plus pre-corrected), parts per million */

    double sum_tweak_sum;       /* cumulative error (just pre-corrected) */
    double sum_sntp_error;      /* cumulative error (just SNTP adjtime) */

    /*
     * statistics from least squares linear regression of (error_ppm,
     * seconds) data
     */

    double stat_ppm;            /* error mean, parts per million */
    double stat_stderr;         /* standard error of error mean, parts per million */
    unsigned int stat_n;        /* number in population */
    unsigned int stat_n_valid;  /* number in population that haven't been discarded */

    unsigned int converging_count;  /* count of sequential converging stats */
    unsigned int flash_writes;  /* count of flash writes */
};

void mqtt_start(const char *broker_host, const char *site_room);

void mqtt_stop(void);

void mqtt_pub_sensors_once(unsigned int period, uint8_t n);

void mqtt_pub_sensors(const time_t * t, const struct tm *timeinfo, float rh,
    float temp_c, uint8_t n) __attribute__((nonnull(1, 2)));

void mqtt_pub_statistics(const struct mqtt_stats_s *stats)
    __attribute__((nonnull(1)));

void mqtt_connected_cb(void);

void mqtt_disconnected_cb(void);
