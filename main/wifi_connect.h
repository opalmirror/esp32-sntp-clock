/* Common functions for establishing Wi-Fi connection.

   Derived from FreeRTOS example_connect.h

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
 */

#pragma once

#ifdef __cplusplus
extern "C"
{
#endif
#if 0                           /* placate indent */
}
#endif

#define CONFIG_EXAMPLE_CONNECT_WIFI 1
#define CONFIG_EXAMPLE_WIFI_SCAN_METHOD_FAST 1
// #define CONFIG_EXAMPLE_WIFI_SCAN_METHOD_ALL_CHANNEL 1
#define CONFIG_EXAMPLE_WIFI_SCAN_RSSI_THRESHOLD -127
#define CONFIG_WIFI_AUTH_OPEN 1
// #define CONFIG_EXAMPLE_WIFI_AUTH_WEP 1
// #define CONFIG_EXAMPLE_WIFI_AUTH_WPA_PSK 1
// #define CONFIG_EXAMPLE_WIFI_AUTH_WPA2_PSK 1
// #define CONFIG_EXAMPLE_WIFI_AUTH_WPA_WPA2_PSK 1
// #define CONFIG_EXAMPLE_WIFI_AUTH_WPA2_ENTERPRISE 1
// #define CONFIG_EXAMPLE_WIFI_AUTH_WPA3_PSK 1
// #define CONFIG_EXAMPLE_WIFI_AUTH_WPA2_WPA3_PSK 1
// #define CONFIG_EXAMPLE_WIFI_AUTH_WAPI_PSK 1
#define CONFIG_EXAMPLE_WIFI_CONNECT_AP_BY_SIGNAL 1
// #define CONFIG_EXAMPLE_WIFI_CONNECT_AP_BY_SECURITY 1
// #define CONFIG_EXAMPLE_CONNECT_ETHERNET 1
#define CONFIG_EXAMPLE_CONNECT_IPV6 1
#define CONFIG_EXAMPLE_CONNECT_IPV6_PREF_LOCAL_LINK 1
// #define CONFIG_EXAMPLE_CONNECT_IPV6_PREF_GLOBAL 1
// #define CONFIG_EXAMPLE_CONNECT_IPV6_PREF_SITE_LOCAL 1
// #define CONFIG_EXAMPLE_CONNECT_IPV6_PREF_UNIQUE_LOCAL 1

#define EXAMPLE_WIFI_SCAN_AUTH_MODE_THRESHOLD 0

/**
 * @brief Configure Wi-Fi, connect, wait for IP
 *
 * @return ESP_OK on successful connection
 */
esp_err_t wifi_connect(const char *ssid, const char *password);

/**
 * Counterpart to example_connect, de-initializes Wi-Fi
 */
esp_err_t wifi_disconnect(void);

/**
 * @brief Returns esp-netif pointer created by wifi_connect()
 */
esp_netif_t *get_wifi_netif(void);

/**
 * @brief Returns esp-netif pointer created by example_connect() described by
 * the supplied desc field
 *
 * @param desc Textual interface of created network interface, for example "sta"
 * indicate default WiFi station.
 *
 */
esp_netif_t *get_wifi_netif_from_desc(const char *desc);

#if 0                           /* placate indent */
{
#endif
#ifdef __cplusplus
}
#endif
