#
# "main" pseudo-component makefile.
#
# (Uses default behaviour of compiling all source files in directory, adding 'include' to include path.)
main/button.o: CFLAGS += -Wall -Wextra -Wpedantic
main/clock_i2c.o: CFLAGS += -Wall -Wextra -Wpedantic
main/clock_main.o: CFLAGS += -Wall -Wextra -Wpedantic
main/flash.o: CFLAGS += -Wall -Wextra -Wpedantic
main/ht16k33.o: CFLAGS += -Wall -Wextra -Wpedantic
main/htu21df.o: CFLAGS += -Wall -Wextra -Wpedantic
main/mqtt_tcp.o: CFLAGS += -Wall -Wextra -Wpedantic
main/stats.o: CFLAGS += -Wall -Wextra -Wpedantic
main/ticks.o: CFLAGS += -Wall -Wextra -Wpedantic
main/tv.o: CFLAGS += -Wall -Wextra -Wpedantic
main/wifi_connect.o: CFLAGS += -Wall -Wextra -Wpedantic
