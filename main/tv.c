/* SNTP LED clock application, timeval functions

*/

/* FreeRTOS includes */
#include "freertos/FreeRTOS.h"

/* Application includes */
#include "tv.h"

enum
{
    US_PER_S = 1000000L,
};

int64_t us_to_s(int64_t us)
{
    return us / US_PER_S;
}

int64_t s_to_us(int64_t s)
{
    return s * US_PER_S;
}

int64_t tv_to_us(const struct timeval *tv)
{
    return s_to_us(tv->tv_sec) + tv->tv_usec;
}

void us_to_tv(int64_t us, struct timeval *tv)
{
    tv->tv_sec = us_to_s(us);
    tv->tv_usec = us - s_to_us(tv->tv_sec);
}

double tv_to_double(const struct timeval *tv)
{
    return tv->tv_sec + 1e-6 * tv->tv_usec;
}

void double_to_tv(double d, struct timeval *tv)
{
    tv->tv_sec = d;
    tv->tv_usec = d - tv->tv_sec;
}

void tv_zero(struct timeval *tv)
{
    tv->tv_sec = 0;
    tv->tv_usec = 0;
}

int tv_cmp(const struct timeval *t, const struct timeval *u)
{
    return
        (t->tv_sec < u->tv_sec) ? -1 :
        (t->tv_sec > u->tv_sec) ? 1 :
        (t->tv_usec < u->tv_usec) ? -1 : (t->tv_usec > u->tv_usec) ? 1 : 0;
}

void tv_negate(const struct timeval *t, struct timeval *result)
{
    result->tv_sec = -t->tv_sec;
    result->tv_usec = -t->tv_usec;
}

void tv_add(const struct timeval *t, const struct timeval *u,
    struct timeval *result)
{
    result->tv_sec = t->tv_sec + u->tv_sec;
    result->tv_usec = t->tv_usec + u->tv_usec;

    /*
     * Make signs of tv_sec and tv_usec match:
     *   -2,  999999 ->  -1, -1999999
     *    1, -999999 ->   0,        1
     */

    if (result->tv_sec < 0 && result->tv_usec > 0)
    {
        result->tv_usec -= US_PER_S;
        result->tv_sec++;
    }
    else if (result->tv_sec > 0 && result->tv_usec < 0)
    {
        result->tv_usec += US_PER_S;
        result->tv_sec--;
    }

    /*
     * adjust tv_sec so tv_usec will be in range (-US_PER_S + 1, US_PER_S - 1)
     */

    if (result->tv_usec <= -US_PER_S)
    {
        result->tv_usec += US_PER_S;
        result->tv_sec--;
    }
    else if (result->tv_usec >= US_PER_S)
    {
        result->tv_usec -= US_PER_S;
        result->tv_sec++;
    }
}

void tv_subtract(const struct timeval *t, const struct timeval *u,
    struct timeval *result)
{
    struct timeval v;

    tv_negate(u, &v);
    tv_add(t, &v, result);
}
