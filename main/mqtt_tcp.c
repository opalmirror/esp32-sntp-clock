/* SNTP LED clock application, common MQTT over TCP functions

   Derived from esp-idf/exmaples/protocols/mqtt/tcp/app_main.c

*/

/* C library includes */
#include <time.h>               // localtime_r, strptime

/* FreeRTOS includes */
#include "freertos/FreeRTOS.h"

/* ESP32 includes */
#include "esp_log.h"
#include "mqtt_client.h"

/* Application includes */
#include "defaults.h"
#include "mqtt_tcp.h"
#include "ticks.h"

/* TODO make more object-oriented esp. wrt thread data */

enum
{
    RETAIN = 1,
};

typedef struct
{
    bool retain;
    char topic[50];
    char data[30];
} q_elem_t;

bool run_thread = true;
QueueHandle_t q;
esp_mqtt_client_config_t mqtt_cfg = {
    .uri = "mqtt://" SENSORS_MQTT_BROKER_DEF,
};

bool mqtt_off = false;
bool mqtt_connected = false;

static const char *TAG = "mqtt_tcp";
static const char *room = SENSORS_MQTT_SITE_ROOM_DEF;

static TaskHandle_t mqtt_thread_handle;

static esp_mqtt_client_handle_t mqtt_client;

static void log_error_if_nonzero(const char *message, int error_code)
{
    if (error_code != 0)
    {
        ESP_LOGE(TAG, "Last error %s: 0x%x", message, error_code);
    }
}

/*
 * @brief Event handler registered to receive MQTT events
 *
 *  This function is called by the MQTT client event loop.
 *
 * @param handler_args user data registered to the event.
 * @param base Event base for the handler(always MQTT Base in this example).
 * @param event_id The id for the received event.
 * @param event_data The data for the event, esp_mqtt_event_handle_t.
 */
static void mqtt_event_handler(void *handler_args, esp_event_base_t base,
    int32_t event_id, void *event_data)
{
    ESP_LOGD(TAG, "Event dispatched from event loop base=%s, event_id=%d", base,
        event_id);

    esp_mqtt_event_handle_t event = event_data;

    switch ((esp_mqtt_event_id_t) event_id)
    {

        case MQTT_EVENT_CONNECTED:
            ESP_LOGI(TAG, "MQTT_EVENT_CONNECTED");
            mqtt_connected = true;
            mqtt_connected_cb();
            break;

        case MQTT_EVENT_DISCONNECTED:
            mqtt_connected = false;
            ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");
            mqtt_disconnected_cb();
            break;

        case MQTT_EVENT_SUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
            break;

        case MQTT_EVENT_UNSUBSCRIBED:
            ESP_LOGI(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
            break;

        case MQTT_EVENT_PUBLISHED:
            ESP_LOGI(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
            break;

        case MQTT_EVENT_DATA:  /* content recieved on subscribed topic(s) */
            ESP_LOGI(TAG, "MQTT_EVENT_DATA");
            printf("TOPIC=%.*s\r\n", event->topic_len, event->topic);
            /* TODO parse document */
            printf("DATA=%.*s\r\n", event->data_len, event->data);

            /* TODO handle subscribed topic(s) - config change, commands, etc. */
            break;

        case MQTT_EVENT_ERROR:
            ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
            if (event->error_handle->error_type ==
                MQTT_ERROR_TYPE_TCP_TRANSPORT)
            {
                log_error_if_nonzero("reported from esp-tls",
                    event->error_handle->esp_tls_last_esp_err);
                log_error_if_nonzero("reported from tls stack",
                    event->error_handle->esp_tls_stack_err);
                log_error_if_nonzero("captured as transport's socket errno",
                    event->error_handle->esp_transport_sock_errno);
                ESP_LOGI(TAG, "Last errno string (%s)",
                    strerror(event->error_handle->esp_transport_sock_errno));

            }
            break;

        default:
            ESP_LOGI(TAG, "Other event id:%d", event->event_id);
            break;

    }
}

static void mqtt_thread(void *params)
{

    mqtt_client = esp_mqtt_client_init(&mqtt_cfg);

    esp_mqtt_client_register_event(mqtt_client, ESP_EVENT_ANY_ID,
        mqtt_event_handler, NULL);

    /* TODO connect pending = true */
    esp_mqtt_client_start(mqtt_client);

    q_elem_t msg;
    const unsigned int TIMEOUT_TICKS = 100;

    while (run_thread)
    {
        /* TODO wait on multiple events: mqtt_connected or q not empty or timeout */

        if (!mqtt_connected)    /* && connect pending */
        {
            /* wait a bit and try again */
            vTaskDelay(ms_to_ticks(100));
            continue;
        }

        /* TODO
         * if connect pending
         *   subscribe topics
         *   connect pending = false
         */

        if (xQueueReceive(q, (void *)&msg, TIMEOUT_TICKS) == pdTRUE)
        {
            esp_mqtt_client_publish(mqtt_client, msg.topic, msg.data, 0, 0,
                msg.retain);
        }
        else
        {
            /* timeout */
        }

        /* TODO anything else here? */
    }

    /*
     * TODO need orderly client teardown here:
     * if connected
     *   if subscribed to topics
     *     unsubscribe
     *   disconnect server
     *   wait for pending disconnect
     * unstart
     * unregister_event
     * uninit
     */

    vQueueDelete(q);
    vTaskDelete(NULL);
}

void mqtt_start(const char *broker_host, const char *site_room)
{
    const unsigned int queue_size = 20;
    const unsigned int STACK_SIZE = 4 * 1024;
    static bool first_time = true;

    if (strcmp(broker_host, "off") == 0)
    {
        mqtt_off = true;
        return;
    }

    /* TODO implement mqtt_stop */
    if (!first_time)
    {
        return;
    }

    first_time = false;

    esp_log_level_set("*", ESP_LOG_INFO);
    esp_log_level_set("MQTT_CLIENT", ESP_LOG_VERBOSE);
    esp_log_level_set("MQTT_EXAMPLE", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT_BASE", ESP_LOG_VERBOSE);
    esp_log_level_set("esp-tls", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT", ESP_LOG_VERBOSE);
    esp_log_level_set("OUTBOX", ESP_LOG_VERBOSE);

    if (broker_host)
    {
        char broker_url[80];

        snprintf(broker_url, sizeof(broker_url), "mqtt://%s", broker_host);

        if (strcmp(broker_url, mqtt_cfg.uri) != 0)
        {
            mqtt_cfg.uri = strdup(broker_url);
        }
    }

    if (site_room)
    {
        room = strdup(site_room);
    }

    q = xQueueCreate(queue_size, sizeof(q_elem_t));

    BaseType_t xRet = xTaskCreate(mqtt_thread, "tmqtt", STACK_SIZE, NULL,
        tskIDLE_PRIORITY + 1, &mqtt_thread_handle);

    if (xRet != pdPASS)
    {
        ESP_LOGE(TAG, "mqtt_thread: create failed");
    }
}

static void mqtt_pub_s(const char *topic_base, const char *subtopic,
    const char *data, int retain)
{
    q_elem_t msg;

    snprintf(msg.topic, sizeof(msg.topic), "%s/%s", topic_base, subtopic);
    snprintf(msg.data, sizeof(msg.data), "%s", data);
    msg.retain = retain;
    xQueueSendToBack(q, &msg, 0);
}

static void mqtt_pub_lu(const char *topic_base, const char *subtopic,
    unsigned long data, int retain)
{
    q_elem_t msg;

    snprintf(msg.topic, sizeof(msg.topic), "%s/%s", topic_base, subtopic);
    snprintf(msg.data, sizeof(msg.data), "%lu", data);
    msg.retain = retain;
    xQueueSendToBack(q, &msg, 0);
}

static void mqtt_pub_f(const char *topic_base, const char *subtopic, float data,
    unsigned int precision, int retain)
{
    q_elem_t msg;
    char fmt[10];

    snprintf(msg.topic, sizeof(msg.topic), "%s/%s", topic_base, subtopic);
    snprintf(fmt, sizeof(fmt), "%%.%uf", precision);
    snprintf(msg.data, sizeof(msg.data), fmt, data);
    msg.retain = retain;
    xQueueSendToBack(q, &msg, 0);
}

static void mqtt_pub_sensor_once(const char *topic_base,
    const char *sensor_name, const char *description, unsigned int period)
{
    char topic[80];

    snprintf(topic, sizeof(topic), "%s/%s", topic_base, sensor_name);
    mqtt_pub_s(topic, "description", description, RETAIN);
    mqtt_pub_lu(topic, "period", period, RETAIN);
}

void mqtt_pub_sensors_once(unsigned int period, uint8_t n)
{
    if (mqtt_off)
    {
        return;
    }

    char sensor_base[80];

    snprintf(sensor_base, sizeof(sensor_base), "/%s/sensor", room);

    if (n == 0)
    {
        mqtt_pub_sensor_once(sensor_base, "rh", "Relative Humidity (%)",
            period);
        mqtt_pub_sensor_once(sensor_base, "temp", "Temperature (°C)", period);
    }
    else if (n == 1)
    {
        mqtt_pub_sensor_once(sensor_base, "rh1", "Relative Humidity (%)",
            period);
        mqtt_pub_sensor_once(sensor_base, "temp1", "Temperature (°C)", period);
    }
}

static void mqtt_pub_sensor(const char *topic_base, const char *sensor_name,
    const time_t * t, float data)
{
    if (mqtt_off)
    {
        return;
    }

    char topic[80];

    snprintf(topic, sizeof(topic), "%s/%s", topic_base, sensor_name);
    mqtt_pub_lu(topic, "seconds", *t, RETAIN);
    mqtt_pub_f(topic, "data", data, 2, RETAIN);
}

void mqtt_pub_sensors(const time_t * t, const struct tm *timeinfo, float rh,
    float temp_c, uint8_t n)
{
    if (mqtt_off)
    {
        return;
    }

    char sensor_base[80];

    snprintf(sensor_base, sizeof(sensor_base), "/%s/sensor", room);

    if (n == 0)
    {
        mqtt_pub_sensor(sensor_base, "rh", t, rh);
        mqtt_pub_sensor(sensor_base, "temp", t, temp_c);
    }
    else if (n == 1)
    {
        mqtt_pub_sensor(sensor_base, "rh1", t, rh);
        mqtt_pub_sensor(sensor_base, "temp1", t, temp_c);
    }
}

void mqtt_pub_statistics(const struct mqtt_stats_s *stats)
{
    if (mqtt_off)
    {
        return;
    }

    char statistic_base[80];

    snprintf(statistic_base, sizeof(statistic_base), "/%s/stat", room);

    mqtt_pub_lu(statistic_base, "seconds", stats->now.tv_sec, 0);
    mqtt_pub_lu(statistic_base, "microseconds", stats->now.tv_usec, 0);

    mqtt_pub_f(statistic_base, "elapsed", stats->elapsed, 6, 0);
    mqtt_pub_f(statistic_base, "sntp_error", stats->sntp_error, 6, 0);
    mqtt_pub_f(statistic_base, "tweak_sum", stats->tweak_sum, 6, 0);
    mqtt_pub_f(statistic_base, "tweak_ppm", stats->tweak_ppm, 3, 0);
    mqtt_pub_f(statistic_base, "error", stats->error, 6, 0);
    mqtt_pub_f(statistic_base, "ppm", stats->ppm, 3, 0);

    mqtt_pub_f(statistic_base, "sum_tweak_sum", stats->sum_tweak_sum, 6, 0);
    mqtt_pub_f(statistic_base, "sum_sntp_error", stats->sum_sntp_error, 6, 0);

    mqtt_pub_f(statistic_base, "stat_ppm", stats->stat_ppm, 3, 0);
    mqtt_pub_f(statistic_base, "stat_stderr", stats->stat_stderr, 3, 0);
    mqtt_pub_lu(statistic_base, "stat_n", stats->stat_n, 0);
    mqtt_pub_lu(statistic_base, "stat_n_valid", stats->stat_n_valid, 0);

    mqtt_pub_lu(statistic_base, "converging_count", stats->converging_count, 0);
    mqtt_pub_lu(statistic_base, "flash_writes", stats->flash_writes, 0);
}

void mqtt_stop(void)
{
    if (mqtt_off)
    {
        return;
    }

    /* TODO tell thread to stop running, wait for thread exited */
}
