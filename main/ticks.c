/* SNTP LED clock with temperature/humidity sensor, tick helper functions

*/

/* FreeRTOS and EspressIF includes */
#include "freertos/FreeRTOS.h"

/* application includes */
#include "ticks.h"

/**************************************
 * public functions
 **************************************/

/* convert ms, rounding up, to ticks */
int ms_to_ticks(int x)
{
    return (x + portTICK_PERIOD_MS - 1) / portTICK_PERIOD_MS;
}
