#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "nvs.h"

#include <string.h>

#include "clock_i2c.h"
#include "defaults.h"
#include "ht16k33.h"
#include "flash.h"

static const char *TAG = "flash";

/* Change this to force defaults */
static const uint32_t APP_INIT_MAGIC = (uint32_t) 0x65666768;

static bool flash_healthy = false;

static void flash_write_key_str(nvs_handle_t h, const char *key, const char *p)
{
    char s[FLASH_MAX_STR_SIZE];
    size_t slen = FLASH_MAX_STR_SIZE;

    ESP_ERROR_CHECK(nvs_get_str(h, key, s, &slen));

    if (strcmp(s, p) != 0)
    {
        ESP_ERROR_CHECK(nvs_set_str(h, key, p));
    }
}

static void flash_write_key_u8(nvs_handle_t h, const char *key, uint8_t n)
{
    uint8_t u;

    ESP_ERROR_CHECK(nvs_get_u8(h, key, &u));

    if (u != n)
    {
        ESP_ERROR_CHECK(nvs_set_u8(h, key, n));
    }
}

static void flash_write_key_u16(nvs_handle_t h, const char *key, uint16_t n)
{
    uint16_t u;

    ESP_ERROR_CHECK(nvs_get_u16(h, key, &u));

    if (u != n)
    {
        ESP_ERROR_CHECK(nvs_set_u16(h, key, n));
    }
}

static void flash_write_key_i16(nvs_handle_t h, const char *key, int16_t n)
{
    int16_t u;

    ESP_ERROR_CHECK(nvs_get_i16(h, key, &u));

    if (u != n)
    {
        ESP_ERROR_CHECK(nvs_set_i16(h, key, n));
    }
}

static void flash_write_key_u32(nvs_handle_t h, const char *key, uint32_t n)
{
    uint32_t u;

    ESP_ERROR_CHECK(nvs_get_u32(h, key, &u));

    if (u != n)
    {
        ESP_ERROR_CHECK(nvs_set_u32(h, key, n));
    }
}

static void flash_write_key_i32(nvs_handle_t h, const char *key, int32_t n)
{
    int32_t u;

    ESP_ERROR_CHECK(nvs_get_i32(h, key, &u));

    if (u != n)
    {
        ESP_ERROR_CHECK(nvs_set_i32(h, key, n));
    }
}

static void flash_init_defaults(flash_data * p)
{
    ESP_LOGI(TAG, "%s", __func__);

    memset(p, 0, sizeof(*p));

    snprintf(p->wifi.hostname, sizeof(p->wifi.hostname), "%s",
        WIFI_HOSTNAME_DEF);
    snprintf(p->wifi.ssid, sizeof(p->wifi.ssid), "%s", WIFI_SSID_DEF);
    snprintf(p->wifi.password, sizeof(p->wifi.password), "%s",
        WIFI_PASSWORD_DEF);

    snprintf(p->sensors.mqtt_broker, sizeof(p->sensors.mqtt_broker), "%s",
        SENSORS_MQTT_BROKER_DEF);
    snprintf(p->sensors.mqtt_site_room, sizeof(p->sensors.mqtt_site_room), "%s",
        SENSORS_MQTT_SITE_ROOM_DEF);
    p->sensors.period_s = SENSORS_PERIOD_S_DEF;

    snprintf(p->sntp.server, sizeof(p->sntp.server), "%s", SNTP_SERVER_DEF);
    p->sntp.interval_min_s = SNTP_INTERVAL_MIN_S_DEF;
    p->sntp.interval_max_s = SNTP_INTERVAL_MAX_S_DEF;
    p->sntp.clock_error_ppm_max = SNTP_CLOCK_ERROR_PPM_MAX_DEF;
    p->sntp.clock_error_stderr_max = SNTP_CLOCK_ERROR_STDERR_MAX_DEF;

    p->display.flags = DISPLAY_FLAGS_DEF;
    p->display.brightness = DISPLAY_BRIGHTNESS_DEF;
    p->display.tz_off_min = DISPLAY_TZ_OFF_MIN_DEF;
    snprintf(p->display.tz, sizeof(p->display.tz), "%s", DISPLAY_TZ_DEF);
}

void flash_init(void)
{
    esp_err_t err = nvs_flash_init();
    bool need_init = err == ESP_ERR_NVS_NO_FREE_PAGES ||
        err == ESP_ERR_NVS_NEW_VERSION_FOUND;

    if (need_init)
    {
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }

    ESP_ERROR_CHECK(err);

    nvs_handle_t h;

    err = nvs_open("storage", NVS_READWRITE, &h);

    if (err != ESP_OK)
    {
        ESP_LOGE(TAG, "%s: nvs_open failed: %s", __func__,
            esp_err_to_name(err));
        return;                 /* Fatal, I think */
    }

    uint32_t app_init = 0x0;

    err = nvs_get_u32(h, "app_init", &app_init);
    nvs_close(h);

    switch (err)
    {
        case ESP_OK:
            if (app_init != APP_INIT_MAGIC)
            {
                ESP_LOGW(TAG, "%s: app_init 0x%08x != 0x%08x", __func__,
                    app_init, APP_INIT_MAGIC);
                need_init = true;
            }
            break;

        case ESP_ERR_NVS_NOT_FOUND:
            ESP_LOGW(TAG, "%s: app_init key not found", __func__);
            need_init = true;
            break;

        default:
            ESP_LOGE(TAG, "%s: error reading app_init: %s", __func__,
                esp_err_to_name(err));

            flash_healthy = false;
            return;             /* Fatal, I think */
    }

    ESP_LOGI(TAG, "%s: app_init=0x%08x", __func__, app_init);

    if (!need_init)
    {
        flash_healthy = true;
        return;
    }

    ESP_LOGW(TAG, "%s: initializing storage", __func__);

    ESP_ERROR_CHECK(nvs_flash_erase());
    ESP_ERROR_CHECK(nvs_flash_init());
    ESP_ERROR_CHECK(nvs_open("storage", NVS_READWRITE, &h));

    app_init = APP_INIT_MAGIC;

    err = nvs_set_u32(h, "app_init", app_init);

    if (err != ESP_OK)
    {
        ESP_LOGE(TAG, "%s: error writing app_init: %s", __func__,
            esp_err_to_name(err));

        flash_healthy = false;
        nvs_close(h);
        return;
    }

    err = nvs_commit(h);

    if (err != ESP_OK)
    {
        ESP_LOGE(TAG, "%s: error committing app_init: %s", __func__,
            esp_err_to_name(err));

        flash_healthy = false;
        nvs_close(h);
        return;
    }

    flash_healthy = true;
    nvs_close(h);

#if 1
    ESP_ERROR_CHECK(nvs_open("storage", NVS_READWRITE, &h));

    ESP_ERROR_CHECK(nvs_get_u32(h, "app_init", &app_init));
    nvs_close(h);
    ESP_LOGI(TAG, "%s: app_init 0x%08x", __func__, app_init);
#endif

    flash_data d;
    flash_data *p = &d;

    flash_init_defaults(&d);

    ESP_ERROR_CHECK(nvs_open("storage", NVS_READWRITE, &h));

    ESP_ERROR_CHECK(nvs_set_str(h, FLASH_WIFI_HOSTNAME, p->wifi.hostname));
    ESP_ERROR_CHECK(nvs_set_str(h, FLASH_WIFI_SSID, p->wifi.ssid));
    ESP_ERROR_CHECK(nvs_set_str(h, FLASH_WIFI_PASSWORD, p->wifi.password));

    ESP_ERROR_CHECK(nvs_set_str(h, FLASH_SENSORS_BROKER,
            p->sensors.mqtt_broker));
    ESP_ERROR_CHECK(nvs_set_str(h, FLASH_SENSORS_SITERM,
            p->sensors.mqtt_site_room));
    ESP_ERROR_CHECK(nvs_set_u16(h, FLASH_SENSORS_PERIOD, p->sensors.period_s));

    ESP_ERROR_CHECK(nvs_set_str(h, FLASH_SNTP_SERVER, p->sntp.server));
    ESP_ERROR_CHECK(nvs_set_u16(h, FLASH_SNTP_INTERVAL_MIN,
            p->sntp.interval_min_s));
    ESP_ERROR_CHECK(nvs_set_u16(h, FLASH_SNTP_INTERVAL_MAX,
            p->sntp.interval_max_s));
    ESP_ERROR_CHECK(nvs_set_u16(h, FLASH_SNTP_ERRPPM_MAX,
            p->sntp.clock_error_ppm_max));
    ESP_ERROR_CHECK(nvs_set_u16(h, FLASH_SNTP_STDERR_MAX,
            p->sntp.clock_error_stderr_max));
    ESP_ERROR_CHECK(nvs_set_i32(h, FLASH_SNTP_PPB, p->sntp.ppb));
    ESP_ERROR_CHECK(nvs_set_u32(h, FLASH_SNTP_PPB_TIME,
            (uint32_t) p->sntp.ppb_time));

    ESP_ERROR_CHECK(nvs_set_u16(h, FLASH_DISPLAY_FLAGS, p->display.flags));
    ESP_ERROR_CHECK(nvs_set_u8(h, FLASH_DISPLAY_BRIGHT, p->display.brightness));
    ESP_ERROR_CHECK(nvs_set_i16(h, FLASH_DISPLAY_TZ_OFF,
            p->display.tz_off_min));
    ESP_ERROR_CHECK(nvs_set_str(h, FLASH_DISPLAY_TZ, p->display.tz));

    ESP_ERROR_CHECK(nvs_commit(h));

    nvs_close(h);

    ESP_LOGW(TAG, "%s: initialized storage", __func__);
}

void flash_read(flash_data * p)
{
    if (!flash_healthy)
    {
        return;
    }

    nvs_handle_t h;

    ESP_ERROR_CHECK(nvs_open("storage", NVS_READONLY, &h));

    size_t len;

    len = sizeof(p->wifi.hostname);
    ESP_ERROR_CHECK(nvs_get_str(h, FLASH_WIFI_HOSTNAME, p->wifi.hostname,
            &len));

    len = sizeof(p->wifi.ssid);
    ESP_ERROR_CHECK(nvs_get_str(h, FLASH_WIFI_SSID, p->wifi.ssid, &len));

    len = sizeof(p->wifi.password);
    ESP_ERROR_CHECK(nvs_get_str(h, FLASH_WIFI_PASSWORD, p->wifi.password,
            &len));

    len = sizeof(p->sensors.mqtt_broker);
    ESP_ERROR_CHECK(nvs_get_str(h, FLASH_SENSORS_BROKER, p->sensors.mqtt_broker,
            &len));

    len = sizeof(p->sensors.mqtt_site_room);
    ESP_ERROR_CHECK(nvs_get_str(h, FLASH_SENSORS_SITERM,
            p->sensors.mqtt_site_room, &len));

    ESP_ERROR_CHECK(nvs_get_u16(h, FLASH_SENSORS_PERIOD, &p->sensors.period_s));

    len = sizeof(p->sntp.server);
    ESP_ERROR_CHECK(nvs_get_str(h, FLASH_SNTP_SERVER, p->sntp.server, &len));

    ESP_ERROR_CHECK(nvs_get_u16(h, FLASH_SNTP_INTERVAL_MIN,
            &p->sntp.interval_min_s));
    ESP_ERROR_CHECK(nvs_get_u16(h, FLASH_SNTP_INTERVAL_MAX,
            &p->sntp.interval_max_s));
    ESP_ERROR_CHECK(nvs_get_u16(h, FLASH_SNTP_ERRPPM_MAX,
            &p->sntp.clock_error_ppm_max));
    ESP_ERROR_CHECK(nvs_get_u16(h, FLASH_SNTP_STDERR_MAX,
            &p->sntp.clock_error_stderr_max));
    ESP_ERROR_CHECK(nvs_get_i32(h, FLASH_SNTP_PPB, &p->sntp.ppb));
    ESP_ERROR_CHECK(nvs_get_u32(h, FLASH_SNTP_PPB_TIME,
            (uint32_t *) & p->sntp.ppb_time));

    ESP_ERROR_CHECK(nvs_get_u16(h, FLASH_DISPLAY_FLAGS, &p->display.flags));
    ESP_ERROR_CHECK(nvs_get_u8(h, FLASH_DISPLAY_BRIGHT,
            &p->display.brightness));
    ESP_ERROR_CHECK(nvs_get_i16(h, FLASH_DISPLAY_TZ_OFF,
            &p->display.tz_off_min));

    len = sizeof(p->display.tz);
    ESP_ERROR_CHECK(nvs_get_str(h, FLASH_DISPLAY_TZ, p->display.tz, &len));

    nvs_close(h);
}

void flash_write(const flash_data * p)
{
    if (!flash_healthy)
    {
        return;
    }

    nvs_handle_t h;

    ESP_ERROR_CHECK(nvs_open("storage", NVS_READWRITE, &h));

    flash_write_key_str(h, FLASH_WIFI_HOSTNAME, p->wifi.hostname);
    flash_write_key_str(h, FLASH_WIFI_SSID, p->wifi.ssid);
    flash_write_key_str(h, FLASH_WIFI_PASSWORD, p->wifi.password);

    flash_write_key_str(h, FLASH_SENSORS_BROKER, p->sensors.mqtt_broker);
    flash_write_key_str(h, FLASH_SENSORS_SITERM, p->sensors.mqtt_site_room);
    flash_write_key_u16(h, FLASH_SENSORS_PERIOD, p->sensors.period_s);

    flash_write_key_str(h, FLASH_SNTP_SERVER, p->sntp.server);
    flash_write_key_u16(h, FLASH_SNTP_INTERVAL_MIN, p->sntp.interval_min_s);
    flash_write_key_u16(h, FLASH_SNTP_INTERVAL_MAX, p->sntp.interval_max_s);
    flash_write_key_u16(h, FLASH_SNTP_ERRPPM_MAX, p->sntp.clock_error_ppm_max);
    flash_write_key_u16(h, FLASH_SNTP_STDERR_MAX,
        p->sntp.clock_error_stderr_max);
    flash_write_key_i32(h, FLASH_SNTP_PPB, p->sntp.ppb);
    flash_write_key_u32(h, FLASH_SNTP_PPB_TIME, p->sntp.ppb_time);

    flash_write_key_u16(h, FLASH_DISPLAY_FLAGS, p->display.flags);
    flash_write_key_u8(h, FLASH_DISPLAY_BRIGHT, p->display.brightness);
    flash_write_key_i16(h, FLASH_DISPLAY_TZ_OFF, p->display.tz_off_min);
    flash_write_key_str(h, FLASH_DISPLAY_TZ, p->display.tz);

    ESP_ERROR_CHECK(nvs_commit(h));

    nvs_close(h);
}

void flash_show(const flash_data * p)
{
    printf("Flash contents:\n");
    printf("%-15s  %s\n", FLASH_WIFI_HOSTNAME, p->wifi.hostname);
    printf("%-15s  %s\n", FLASH_WIFI_SSID, p->wifi.ssid);
    printf("%-15s  %s\n", FLASH_WIFI_PASSWORD, p->wifi.password);

    printf("%-15s  %s\n", FLASH_SENSORS_BROKER, p->sensors.mqtt_broker);
    printf("%-15s  %s\n", FLASH_SENSORS_SITERM, p->sensors.mqtt_site_room);
    printf("%-15s  %hu seconds \n", FLASH_SENSORS_PERIOD, p->sensors.period_s);

    printf("%-15s  %s\n", FLASH_SNTP_SERVER, p->sntp.server);
    printf("%-15s  %hu seconds\n", FLASH_SNTP_INTERVAL_MIN,
        p->sntp.interval_min_s);
    printf("%-15s  %hu seconds\n", FLASH_SNTP_INTERVAL_MAX,
        p->sntp.interval_max_s);
    printf("%-15s  %hu ppm\n", FLASH_SNTP_ERRPPM_MAX,
        p->sntp.clock_error_ppm_max);
    printf("%-15s  %hu ppm\n", FLASH_SNTP_STDERR_MAX,
        p->sntp.clock_error_stderr_max);
    printf("%-15s  %d ppb\n", FLASH_SNTP_PPB, p->sntp.ppb);
    printf("%-15s  %lu\n", FLASH_SNTP_PPB_TIME, p->sntp.ppb_time);

    printf("%-15s  0x%04x\n", FLASH_DISPLAY_FLAGS, p->display.flags);
    printf("%-15s  %hhu\n", FLASH_DISPLAY_BRIGHT, p->display.brightness);
    printf("%-15s  %hd minutes\n", FLASH_DISPLAY_TZ_OFF, p->display.tz_off_min);
    printf("%-15s  %s\n", FLASH_DISPLAY_TZ, p->display.tz);
}
