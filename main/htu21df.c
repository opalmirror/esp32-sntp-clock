/* SNTP LED clock application, HTU21D(F) chip functions

*/

#include "freertos/FreeRTOS.h"
#include "esp_event.h"
#include "esp_log.h"

#include "clock_i2c.h"
#include "ticks.h"

/**************************************
 * private definitions
 **************************************/

#define HTU21DF_POWERUP_WAIT_MS    15   /*!< From HTU21D(F) Datasheet p.10 */
#define HTU21DF_DEVICE_ADDRESS   0x40   /*!< From HTU21D(F) Datasheet p.10 */
#define HTU21DF_READ_H_PERIOD_MS   16   /*!< From HTU21D(F) Datasheet Table p. 3 */
#define HTU21DF_READ_T_PERIOD_MS   50   /*!< From HTU21D(F) Datasheet Table p. 5 */
#define HTU21DF_RESET_PERIOD_MS    15   /*!< From HTU21D(F) Datasheet Table p. 12 */

/* *INDENT-OFF* */
enum htu21df_registers
{
    HTU21DF_MEAS_RH_HMM       = 0xE5,
    HTU21DF_MEAS_RH_NHMM      = 0xF5,
    HTU21DF_MEAS_TEMP_HMM     = 0xE3,
    HTU21DF_MEAS_TEMP_NHMM    = 0xF3,
    HTU21DF_RESET             = 0xFE,
    HTU21DF_WR_RHT_USER_REG_1 = 0xE6,
    HTU21DF_RD_RHT_USER_REG_1 = 0xE7,
};
/* *INDENT-ON* */

/**************************************
 * private data
 **************************************/

static const char *TAG = "htu21df";

/**************************************
 * public functions
 **************************************/

int htu21df_reset(const i2c_handle h)
{
    const int chip_addr = HTU21DF_DEVICE_ADDRESS;
    const int reg = HTU21DF_RESET;
    int status = -1;

    if (i2c_addr_write(h, chip_addr, reg, NULL, 0) != 0)
    {
        ESP_LOGW(TAG, "%s: fail", __func__);
        goto done;
    }

    vTaskDelay(ms_to_ticks(HTU21DF_RESET_PERIOD_MS));

    status = 0;

 done:
    return status;
}

int htu21df_read_rh(const i2c_handle h, float *prh)
{
    const int chip_addr = HTU21DF_DEVICE_ADDRESS;
    const int reg = HTU21DF_MEAS_RH_HMM;
    const int read_len = 2;
    uint8_t *data = malloc(read_len);
    int status = -1;

    if (i2c_addr_write(h, chip_addr, reg, NULL, 0) != 0)
    {
        ESP_LOGW(TAG, "%s: write fail", __func__);
        goto done;
    }

    vTaskDelay(ms_to_ticks(HTU21DF_READ_H_PERIOD_MS));

    if (i2c_addr_read(h, chip_addr, data, read_len) != 0)
    {
        ESP_LOGW(TAG, "%s: read fail", __func__);
        goto done;
    }

    status = 0;

    int raw = ((int)data[0] << 8) + data[1];
    *prh = 125.0 * raw / 65536.0 - 6.0;

 done:
    free(data);

    return status;
}

int htu21df_read_temp(const i2c_handle h, float *ptemp_c)
{
    const int chip_addr = HTU21DF_DEVICE_ADDRESS;
    const int reg = HTU21DF_MEAS_TEMP_HMM;
    const int read_len = 2;
    uint8_t *data = malloc(read_len);
    int status = -1;

    if (i2c_addr_write(h, chip_addr, reg, NULL, 0) != 0)
    {
        ESP_LOGW(TAG, "%s: write fail", __func__);
        goto done;
    }

    vTaskDelay(ms_to_ticks(HTU21DF_READ_T_PERIOD_MS));

    if (i2c_addr_read(h, chip_addr, data, read_len) != 0)
    {
        ESP_LOGW(TAG, "%s: read fail", __func__);
        goto done;
    }

    status = 0;

    int raw = ((int)data[0] << 8) + data[1];
    *ptemp_c = 175.72 * raw / 65536.0 - 46.85;

 done:
    free(data);

    return status;
}
